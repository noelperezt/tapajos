<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stock_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function ListarStock($filial) {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT Codigo, Nome, Estoque_Minimo, Qtde_Estoque_Atual, Estoque_Ideal
                                    FROM Estoque_Atual
                                    JOIN Prod_Serv ON Estoque_Atual.Ordem_Prod_Serv = Prod_Serv.Ordem
                                    WHERE Prod_Serv.Inativo = 0 AND Ordem_Filial = '.$filial);
        return $query->result_array();
    }

    public function ListarFiliais() {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT Ordem, Nome 
                                    FROM Filiais
                                    ORDER BY Ordem');
        return $query->result_array();
    }


}