<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Financiero_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function diferenciaDias($inicio, $fin){
        $inicio = strtotime($inicio);
        $fin = strtotime($fin);
        $dif = $fin - $inicio;
        $diasFalt = (( ( $dif / 60 ) / 60 ) / 24);
        return ceil($diasFalt);
    }

    public function ConvertirFecha($date){
        $fecha_d_m_y = date("d/m/Y", strtotime($date));
        return $fecha_d_m_y;
    }


    public function DuplicatasTransportadora() {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT Financeiro_Contas.Ordem AS Ordem, Data_Emissao, Situacao, Tipo_Conta, Filiais.Codigo AS Filial, Cli_For.Nome AS Fornecedor, Descricao, Fatura, Nota, Valor_Total, Data_Vencimento, Data_Competencia 
                                    FROM Financeiro_Contas 
                                    JOIN Cli_For ON Ordem_Cli_For = Cli_For.Ordem
                                    JOIN Filiais ON Financeiro_Contas.Ordem_Filial = Filiais.Ordem
                                    WHERE Situacao=\'A\' 
                                    AND Pagar_Receber=\'P\' 
                                    AND Ordem_Plano_Contas3 =14
                                    ORDER BY Data_Vencimento DESC');   
        $datos = $query->result_array();

        $query = $this->db->query('SELECT * 
                                   FROM duplicatas_transportadora');   
        $fecha = $query->result_array();  
        

        for($i=0; $i < count($datos); $i++){
            $resultado[$i]['Ordem'] = $datos[$i]['Ordem'];
            $resultado[$i]['Data_Emissao'] = $this->ConvertirFecha($datos[$i]['Data_Emissao']);
            $resultado[$i]['Situacao'] = $datos[$i]['Situacao'];
            $resultado[$i]['Tipo_Conta'] = $datos[$i]['Tipo_Conta'];
            $resultado[$i]['Filial'] = $datos[$i]['Filial'];
            $resultado[$i]['Fornecedor'] = $datos[$i]['Fornecedor'];
            $resultado[$i]['Descricao'] = $datos[$i]['Descricao'];
            $resultado[$i]['Fatura'] = $datos[$i]['Fatura'];
            $resultado[$i]['Nota'] = $datos[$i]['Nota'];
            $resultado[$i]['Valor_Total'] = number_format($datos[$i]['Valor_Total'], 2, ',', '.');
            $resultado[$i]['Data_Vencimento'] = $this->ConvertirFecha($datos[$i]['Data_Vencimento']);
            $resultado[$i]['Data_Competencia'] = $this->ConvertirFecha($datos[$i]['Data_Competencia']);
            if (empty($fecha)) {
                $resultado[$i]['Data_Previsao'] = "";
                $resultado[$i]['Dias_Restantes'] = 0;
            }else{
                for($j=0; $j < count($fecha); $j++){
                    if($datos[$i]['Ordem'] == $fecha[$j]['ordem']){
                        $resultado[$i]['Data_Previsao'] = $this->ConvertirFecha($fecha[$j]['fecha']);
                        $date = new DateTime();
                        $result = $date->format('Y-m-d');
                        $resultado[$i]['Dias_Restantes'] = $this->diferenciaDias($result, $fecha[$j]['fecha']);
                        break;
                    }else{
                        $resultado[$i]['Data_Previsao'] = "";
                        $resultado[$i]['Dias_Restantes'] = 0;
                    }
                }
            }
           
        }
        
        return $resultado;
    }

    public function RelatorioData($inicio,$fin){
        $inicial = explode("_", $inicio);
        $final  = explode("_", $fin);
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT (SELECT (SELECT ISNULL(sum(preco_final_somado),0)
                                    FROM Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '12:59:00' )
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)) -

                                    (SELECT ISNULL(sum(preco_final_somado),0)
                                    FROM Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '12:59:00' )
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND Ordem_Operacao = 30) AS Total ) AS Manana,
        
                                    (SELECT (SELECT ISNULL(sum(preco_final_somado),0)
                                    FROM Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '13:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)) -

                                    (SELECT ISNULL(sum(preco_final_somado),0)
                                    FROM Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '13:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND Ordem_Operacao = 30) AS Total ) AS Tarde,
        
                                    (SELECT (SELECT ISNULL(sum(preco_final_somado),0)
                                    FROM Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)) -

                                    (SELECT ISNULL(sum(preco_final_somado),0)
                                    FROM Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND Ordem_Operacao = 30) AS Total ) AS Total");
        $data = $query->result_array();
        $resultado['General'] = $data;

        $query = $sqlserver->query("SELECT Tabelas_Preco.Nome, 
                                  ((SELECT SUM(Movimento_Prod_Serv.Preco_Final)
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '12:59:00' )
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                    AND linha_Excluida = 0
                                    AND Movimento_Prod_Serv.Ordem_Tabela_Preco = Tabelas_Preco.Ordem)-(
                                    SELECT ISNULL(SUM(Movimento_Prod_Serv.Preco_Final),0)
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '12:59:00' )
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND Ordem_Operacao = 30
                                    AND linha_Excluida = 0
                                    AND Movimento_Prod_Serv.Ordem_Tabela_Preco = Tabelas_Preco.Ordem
                                    )) AS Manana,
                                   ((SELECT SUM(Movimento_Prod_Serv.Preco_Final)
                                     FROM Movimento
                                     JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                     WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                     AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '13:00:00' AND '19:00:00' )
                                     AND Efetivado_Financeiro = 1 
                                     AND Efetivado_Estoque=1 
                                     AND Desefetivado_Financeiro = 0 
                                     AND Desefetivado_Estoque = 0 
                                     AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                     AND linha_Excluida = 0
                                     AND Movimento_Prod_Serv.Ordem_Tabela_Preco = Tabelas_Preco.Ordem)-(
                                     SELECT ISNULL(SUM(Movimento_Prod_Serv.Preco_Final),0)
                                     FROM Movimento
                                     JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                     WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                     AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '13:00:00' AND '19:00:00' )
                                     AND Efetivado_Financeiro = 1 
                                     AND Efetivado_Estoque=1 
                                     AND Desefetivado_Financeiro = 0 
                                     AND Desefetivado_Estoque = 0 
                                     AND Ordem_Operacao = 30
                                     AND linha_Excluida = 0
                                     AND Movimento_Prod_Serv.Ordem_Tabela_Preco = Tabelas_Preco.Ordem
                                     )) AS Tarde, 
                                    ((SELECT SUM(Movimento_Prod_Serv.Preco_Final)
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                    AND linha_Excluida = 0
                                    AND Movimento_Prod_Serv.Ordem_Tabela_Preco = Tabelas_Preco.Ordem)-(
                                    SELECT ISNULL(SUM(Movimento_Prod_Serv.Preco_Final),0)
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND Ordem_Operacao = 30
                                    AND linha_Excluida = 0
                                    AND Movimento_Prod_Serv.Ordem_Tabela_Preco = Tabelas_Preco.Ordem
                                    )) AS Total
                                    FROM Tabelas_Preco
                                    WHERE Inativo = 0");
        $data = $query->result_array();
        $resultado['Detalle'] = $data;

        $query = $sqlserver->query("SELECT Filiais.Ordem, Filiais.Nome, 
                                  ((SELECT SUM(Movimento_Prod_Serv.Preco_Final)
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '12:59:00' )
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                    AND linha_Excluida = 0
                                    AND Movimento.Ordem_Filial= Filiais.Ordem)-(
                                    SELECT ISNULL(SUM(Movimento_Prod_Serv.Preco_Final),0)
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '12:59:00' )
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND Ordem_Operacao = 30
                                    AND linha_Excluida = 0
                                    AND Movimento.Ordem_Filial= Filiais.Ordem
                                    )) AS Manana,
                                    ((SELECT SUM(Movimento_Prod_Serv.Preco_Final)
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '13:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                    AND linha_Excluida = 0
                                    AND Movimento.Ordem_Filial= Filiais.Ordem)-(
                                    SELECT ISNULL(SUM(Movimento_Prod_Serv.Preco_Final),0)
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '13:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND Ordem_Operacao = 30
                                    AND linha_Excluida = 0
                                    AND Movimento.Ordem_Filial= Filiais.Ordem
                                    )) AS Tarde, 
                                    ((SELECT SUM(Movimento_Prod_Serv.Preco_Final)
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                    AND linha_Excluida = 0
                                    AND Movimento.Ordem_Filial= Filiais.Ordem)-(
                                    SELECT ISNULL(SUM(Movimento_Prod_Serv.Preco_Final),0)
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND Ordem_Operacao = 30
                                    AND linha_Excluida = 0
                                    AND Movimento.Ordem_Filial= Filiais.Ordem
                                    )) AS Total
                                    FROM Filiais
                                    WHERE Filiais.Ordem = 1 OR Filiais.Ordem = 3 OR Filiais.Ordem = 4 OR Filiais.Ordem = 10
                                    ORDER BY Total DESC");
        $data = $query->result_array();
        $resultado['Filiais'] = $data;

        return $resultado;
    }

    public function RelatorioHora($inicio,$fin){
        $inicial = explode("_", $inicio);
        $final  = explode("_", $fin);

        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT(SELECT SUM(Preco_Final_Somado) 
                                    FROM Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '".$inicial[1]."' AND '".$final[1]."')
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7))-
                                    (SELECT SUM(Preco_Final_Somado) 
                                    FROM Movimento
                                    WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '".$inicial[1]."' AND '".$final[1]."')
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND Ordem_Operacao = 30)");
        $data = $query->result_array();
        $resultado['General'] = $data;

        $query = $sqlserver->query("SELECT Tabelas_Preco.Nome AS name, 
                                        ((SELECT CASE WHEN SUM(Movimento_Prod_Serv.Preco_Final) IS NULL THEN 0
                                         ELSE SUM(Movimento_Prod_Serv.Preco_Final)
                                         END
                                         FROM Movimento
                                         JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                         WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                         AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '".$inicial[1]."' AND '".$final[1]."')
                                         AND Efetivado_Financeiro = 1 
                                         AND Efetivado_Estoque=1 
                                         AND Desefetivado_Financeiro = 0 
                                         AND Desefetivado_Estoque = 0 
                                         AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                         AND linha_Excluida = 0
                                         AND Movimento_Prod_Serv.Ordem_Tabela_Preco = Tabelas_Preco.Ordem)-
                                         (SELECT CASE WHEN SUM(Movimento_Prod_Serv.Preco_Final) IS NULL THEN 0
                                         ELSE SUM(Movimento_Prod_Serv.Preco_Final)
                                         END
                                         FROM Movimento
                                         JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                         WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                         AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '".$inicial[1]."' AND '".$final[1]."')
                                         AND Efetivado_Financeiro = 1 
                                         AND Efetivado_Estoque=1 
                                         AND Desefetivado_Financeiro = 0 
                                         AND Desefetivado_Estoque = 0 
                                         AND Ordem_Operacao = 30
                                         AND linha_Excluida = 0
                                         AND Movimento_Prod_Serv.Ordem_Tabela_Preco = Tabelas_Preco.Ordem)) AS value
                                    FROM Tabelas_Preco
                                    WHERE Inativo = 0");
        $data = $query->result_array();
        $resultado['Detalle'] = $data;

        $query = $sqlserver->query("SELECT Filiais.Nome AS name, 
                                        ((SELECT CASE WHEN SUM(Movimento_Prod_Serv.Preco_Final) IS NULL THEN 0
                                         ELSE SUM(Movimento_Prod_Serv.Preco_Final)
                                         END
                                         FROM Movimento
                                         JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                         WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                         AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '".$inicial[1]."' AND '".$final[1]."')
                                         AND Efetivado_Financeiro = 1 
                                         AND Efetivado_Estoque=1 
                                         AND Desefetivado_Financeiro = 0 
                                         AND Desefetivado_Estoque = 0 
                                         AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                         AND linha_Excluida = 0
                                         AND Movimento.Ordem_Filial= Filiais.Ordem)-
                                         (SELECT CASE WHEN SUM(Movimento_Prod_Serv.Preco_Final) IS NULL THEN 0
                                         ELSE SUM(Movimento_Prod_Serv.Preco_Final)
                                         END
                                         FROM Movimento
                                         JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                         WHERE (CONVERT(DATE, Data_Efetivado_Financeiro) BETWEEN '".$inicial[0]."' AND '".$final[0]."'
                                         AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '".$inicial[1]."' AND '".$final[1]."')
                                         AND Efetivado_Financeiro = 1 
                                         AND Efetivado_Estoque=1 
                                         AND Desefetivado_Financeiro = 0 
                                         AND Desefetivado_Estoque = 0 
                                         AND Ordem_Operacao = 30
                                         AND linha_Excluida = 0
                                         AND Movimento.Ordem_Filial= Filiais.Ordem)) AS value
                                    FROM Filiais
                                    WHERE Filiais.Ordem = 1 OR Filiais.Ordem = 3 OR Filiais.Ordem = 4 OR Filiais.Ordem = 10
                                    ORDER BY value DESC");
        $data = $query->result_array();
        $resultado['Filiais'] = $data;

        return $resultado;
    }


    public function CantTotalVencidas() {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT COUNT(*) AS vencidas 
                                    FROM Financeiro_Contas 
                                    JOIN Cli_For ON Ordem_Cli_For = Cli_For.Ordem
                                    JOIN Filiais ON Financeiro_Contas.Ordem_Filial = Filiais.Ordem
                                    WHERE Situacao=\'A\' 
                                    AND Pagar_Receber=\'P\' 
                                    AND Ordem_Plano_Contas3 =14
                                    AND GETDATE() > Data_vencimento ');   
        return $query->result_array();
    }

    public function CantVenceDia() {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT COUNT(*) AS vencidas_dia
                                    FROM Financeiro_Contas 
                                    JOIN Cli_For ON Ordem_Cli_For = Cli_For.Ordem
                                    JOIN Filiais ON Financeiro_Contas.Ordem_Filial = Filiais.Ordem
                                    WHERE Situacao=\'A\' 
                                    AND Pagar_Receber=\'P\' 
                                    AND Ordem_Plano_Contas3 =14
                                    AND GETDATE() = Data_vencimento');   
        return $query->result_array();
    }

    public function CantVenceSemana() {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT COUNT(*) AS vencidas_semana
                                    FROM Financeiro_Contas 
                                    JOIN Cli_For ON Ordem_Cli_For = Cli_For.Ordem
                                    JOIN Filiais ON Financeiro_Contas.Ordem_Filial = Filiais.Ordem
                                    WHERE Situacao=\'A\' 
                                    AND Pagar_Receber=\'P\' 
                                    AND Ordem_Plano_Contas3 =14
                                    AND (DATEPART (WEEK,GETDATE()) = DATEPART (WEEK,Data_vencimento) AND DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_vencimento))');   
        return $query->result_array();
    }

    public function CantVenceMes() {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT COUNT(*) AS vencidas_mes
                                    FROM Financeiro_Contas 
                                    JOIN Cli_For ON Ordem_Cli_For = Cli_For.Ordem
                                    JOIN Filiais ON Financeiro_Contas.Ordem_Filial = Filiais.Ordem
                                    WHERE Situacao=\'A\' 
                                    AND Pagar_Receber=\'P\' 
                                    AND Ordem_Plano_Contas3 =14
                                    AND (DATEPART (MONTH,GETDATE()) = DATEPART (MONTH,Data_vencimento) AND DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_vencimento))');   
        return $query->result_array();
    }

    public function TotalVencidas() {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT SUM(Valor_Total) AS total_vencidas 
                                    FROM Financeiro_Contas 
                                    JOIN Cli_For ON Ordem_Cli_For = Cli_For.Ordem
                                    JOIN Filiais ON Financeiro_Contas.Ordem_Filial = Filiais.Ordem
                                    WHERE Situacao=\'A\' 
                                    AND Pagar_Receber=\'P\' 
                                    AND Ordem_Plano_Contas3 =14
                                    AND GETDATE() > Data_vencimento ');   
        return $query->result_array();
    }

    public function VenceDia() {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT SUM(Valor_Total) AS total_vencidas_dia
                                    FROM Financeiro_Contas 
                                    JOIN Cli_For ON Ordem_Cli_For = Cli_For.Ordem
                                    JOIN Filiais ON Financeiro_Contas.Ordem_Filial = Filiais.Ordem
                                    WHERE Situacao=\'A\' 
                                    AND Pagar_Receber=\'P\' 
                                    AND Ordem_Plano_Contas3 =14
                                    AND GETDATE() = Data_vencimento');   
        return $query->result_array();
    }

    public function VenceSemana() {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT SUM(Valor_Total) AS total_vencidas_semana
                                    FROM Financeiro_Contas 
                                    JOIN Cli_For ON Ordem_Cli_For = Cli_For.Ordem
                                    JOIN Filiais ON Financeiro_Contas.Ordem_Filial = Filiais.Ordem
                                    WHERE Situacao=\'A\' 
                                    AND Pagar_Receber=\'P\' 
                                    AND Ordem_Plano_Contas3 =14
                                    AND (DATEPART (WEEK,GETDATE()) = DATEPART (WEEK,Data_vencimento) AND DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_vencimento))');   
        return $query->result_array();
    }

    public function VenceMes() {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT SUM(Valor_Total) AS total_vencidas_mes
                                    FROM Financeiro_Contas 
                                    JOIN Cli_For ON Ordem_Cli_For = Cli_For.Ordem
                                    JOIN Filiais ON Financeiro_Contas.Ordem_Filial = Filiais.Ordem
                                    WHERE Situacao=\'A\' 
                                    AND Pagar_Receber=\'P\' 
                                    AND Ordem_Plano_Contas3 =14
                                    AND (DATEPART (MONTH,GETDATE()) = DATEPART (MONTH,Data_vencimento) AND DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_vencimento))');   
        return $query->result_array();
    }

}