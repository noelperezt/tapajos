<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Graficas extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function TopVendedor() {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT TOP 5 Ordem_Vendedor1, Funcionarios.Nome, SUM(Preco_Final_Somado) AS Venta,  COUNT(Preco_Final_Somado) AS Cantidad
                                    FROM Movimento JOIN Funcionarios ON Movimento.Ordem_Vendedor1 = Funcionarios.Ordem 
                                    WHERE Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                    AND Ordem_Vendedor1 <> 0 
                                    AND MONTH(Data_Efetivado_Financeiro) = MONTH(GETDATE())
                                    GROUP BY Ordem_Vendedor1, Funcionarios.Nome ORDER BY venta DESC');
        return $query->result_array();
    }

    public function TotalPorAno($ano) {
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT Filiais.Nome, Result_Meses.Nome AS Mes, 
                                    ((SELECT CASE WHEN SUM(Movimento_Prod_Serv.Preco_Final) IS NULL THEN 0
                                    ELSE SUM(Movimento_Prod_Serv.Preco_Final)
                                    END
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE DATEPART (YEAR, Data_Efetivado_Financeiro) = ".$ano."
                                    AND Movimento.Ordem_Filial = Filiais.Ordem
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                    AND linha_Excluida = 0
                                    AND DATEPART (MONTH, Data_Efetivado_Financeiro) = Result_Meses.Ordem)-
                                    (SELECT CASE WHEN SUM(Movimento_Prod_Serv.Preco_Final) IS NULL THEN 0
                                    ELSE SUM(Movimento_Prod_Serv.Preco_Final)
                                    END
                                    FROM Movimento
                                    JOIN Movimento_Prod_Serv ON Movimento.Ordem = Movimento_Prod_Serv.Ordem_Movimento
                                    WHERE DATEPART (YEAR, Data_Efetivado_Financeiro) = ".$ano."
                                    AND Movimento.Ordem_Filial = Filiais.Ordem
                                    AND Efetivado_Financeiro = 1 
                                    AND Efetivado_Estoque=1 
                                    AND Desefetivado_Financeiro = 0 
                                    AND Desefetivado_Estoque = 0 
                                    AND Ordem_Operacao = 30
                                    AND linha_Excluida = 0
                                    AND DATEPART (MONTH, Data_Efetivado_Financeiro) = Result_Meses.Ordem
                                    )) AS Total
                                    FROM Result_Meses,Filiais
                                    WHERE Filiais.Ordem = 1
                                    OR Filiais.Ordem = 3
                                    OR Filiais.Ordem = 4
                                    OR Filiais.Ordem =10");
        $datos = $query->result_array();

        $k = 0;
        for($i=0; $i < count($datos)/12; $i++){
            $aux = $i + 1;
            $aux2 = ($aux * 12) - 1;
            $resultado[$i]['Filial'] = $datos[$aux2]['Nome'];    
            for ($j = 0; $j < 12 ; $j++){
                $consulta[$j]['Mes'] = $datos[$k]['Mes'];
                $consulta[$j]['Total'] = $datos[$k]['Total'];
                $resultado[$i]['Valores'] =  $consulta;
                $k++;
            }
        }

        return $resultado;
    }

    public function ContasPagar(){
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT SUM(Valor_Total) AS Pagar
                                    FROM Financeiro_Contas
                                    WHERE Situacao ='A' 
                                    AND Pagar_Receber ='P' ");
        return $query->result_array();
    }

    public function ContContasPagar(){
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT COUNT(Valor_Total) AS Pagar
                                    FROM Financeiro_Contas
                                    WHERE Situacao ='A' 
                                    AND Pagar_Receber ='P' ");
        return $query->result_array();
    }

    public function ContasReceber(){
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT SUM(Valor_Total) AS Receber
                                    FROM Financeiro_Contas
                                    WHERE Situacao ='A' 
                                    AND Pagar_Receber ='R'
                                    AND Tipo_Conta = 'B'
                                    AND Ordem_Conta_Bancaria = 1 ");
        return $query->result_array();
    }

    public function ContContasReceber(){
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT COUNT(Valor_Total) AS Receber
                                    FROM Financeiro_Contas
                                    WHERE Situacao ='A' 
                                    AND Pagar_Receber ='R'
                                    AND Tipo_Conta = 'B'
                                    AND Ordem_Conta_Bancaria = 1 ");
        return $query->result_array();
    }

    public function VentasMes(){
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT(SELECT ISNULL(SUM(preco_final_somado),0) 
                                    FROM Movimento 
                                    WHERE (DATEPART (MONTH,GETDATE()) = DATEPART (MONTH,Data_Efetivado_Financeiro) AND DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_Efetivado_Financeiro)) 
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' 
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7))-
                                    
                                    (SELECT ISNULL(SUM(preco_final_somado),0) 
                                    FROM Movimento 
                                    WHERE (DATEPART (MONTH,GETDATE()) = DATEPART (MONTH,Data_Efetivado_Financeiro) AND DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_Efetivado_Financeiro)) 
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' 
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND Ordem_Operacao = 30) AS Total_Mes");
        return $query->result_array();
    }

    public function ContVentasMes(){
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT(SELECT ISNULL(COUNT(preco_final_somado),0) 
                                    FROM Movimento 
                                    WHERE (DATEPART (MONTH,GETDATE()) = DATEPART (MONTH,Data_Efetivado_Financeiro) AND DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_Efetivado_Financeiro)) 
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' 
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7))-
                                    
                                    (SELECT ISNULL(COUNT(preco_final_somado),0) 
                                    FROM Movimento 
                                    WHERE (DATEPART (MONTH,GETDATE()) = DATEPART (MONTH,Data_Efetivado_Financeiro) AND DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_Efetivado_Financeiro)) 
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' 
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND Ordem_Operacao = 30) AS Total_Mes");
        return $query->result_array();
    }

    public function VentasAno(){
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT (SELECT ISNULL(SUM(preco_final_somado),0)
                                    FROM Movimento
                                    WHERE (DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_Efetivado_Financeiro)
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)) -

                                    (SELECT ISNULL(SUM(preco_final_somado),0)
                                    FROM Movimento
                                    WHERE (DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_Efetivado_Financeiro)
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND Ordem_Operacao = 30) AS Total_Ano");
        return $query->result_array();
    }

    public function ContVentasAno(){
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT (SELECT ISNULL(COUNT(preco_final_somado),0)
                                    FROM Movimento
                                    WHERE (DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_Efetivado_Financeiro)
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)) -

                                    (SELECT ISNULL(COUNT(preco_final_somado),0)
                                    FROM Movimento
                                    WHERE (DATEPART (YEAR,GETDATE()) = DATEPART (YEAR,Data_Efetivado_Financeiro)
                                    AND CONVERT(TIME, Data_Efetivado_Financeiro) BETWEEN '08:00:00' AND '19:00:00' )
                                    AND Efetivado_Financeiro = 1 AND Efetivado_Estoque=1 AND Desefetivado_Financeiro = 0 AND Desefetivado_Estoque = 0 AND Ordem_Operacao = 30) AS Total_Ano");
        return $query->result_array();
    }


}