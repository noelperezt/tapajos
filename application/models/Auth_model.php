<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /*
     * 
     */

    public function Authentification() {
        $notif = array();
        $username = $this->input->post('username');
        $password = Utils::hash('sha1', $this->input->post('password'), AUTH_SALT);

        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->limit(1);
        

        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $row = $query->row();
            if ($row->is_active != 1) {
                $notif['message'] = 'Your account is disabled !';
                $notif['type'] = 'warning';
            } else {
                $sess_data = array(
                    'users_id' => $row->users_id,
                    'first_name' => $row->first_name,
                    'last_name' => $row->last_name,
                    'email' => $row->email
                );
                $this->session->set_userdata('logged_in', $sess_data, $query);
                $this->update_last_login($row->users_id);
            }
        } else {
            $notif['message'] = 'Username or password incorrect !';
            $notif['type'] = 'danger';
        }

        return $notif;
    }

    /*
     * 
     */

    private function update_last_login($users_id) {
        $sql = "UPDATE users SET last_login = NOW() WHERE users_id=" . $this->db->escape($users_id);
        $this->db->query($sql);
    }

    /*
     * 
     */

    public function register() {
        $notif = array();
        $data = array(
            'username' => $this->input->post('user_name'),
            'first_name' => $this->input->post('nome'),
            'last_name' => $this->input->post('sobrenome'),
            'email' => $this->input->post('email'),
            'password' => Utils::hash('sha1', $this->input->post('password'), AUTH_SALT),
            'is_active' => 1
        );
        $this->db->insert('users', $data);
        $users_id = $this->db->insert_id();
        if ($this->db->affected_rows() > 0) {
            $notif['message'] = 'Saved successfully';
            $notif['type'] = 'success';
            unset($_POST);
        } else {
            $notif['message'] = 'Something wrong !';
            $notif['type'] = 'danger';
        }
        return $notif;
    }

    public function update() {
        $notif = array();
        $id = $this->input->post('id_user');
        $data = array(
            'username' => $this->input->post('user_name'),
            'first_name' => $this->input->post('nome'),
            'last_name' => $this->input->post('sobrenome'),
            'email' => $this->input->post('email')
        );
        $this->db->where('users_id', $id);
        $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0) {
            $notif['message'] = 'Saved successfully';
            $notif['type'] = 'success';
            unset($_POST);
        } else {
            $notif['message'] = 'Something wrong !';
            $notif['type'] = 'danger';
        }
        return $notif;
    }

    public function CambiarStatus($id) {
        $notif = array();
        $query = $this->db->query('SELECT is_active FROM users WHERE users_id ='.$id);
        $resultados = $query->result_array();
        $status = null;

        foreach($resultados as $resultado):
            if($resultado['is_active'] == 1){
                $status = 0;
            }else{
                $status = 1;
            }
        endforeach;
       
        $data = array(
            'is_active' => $status
        );
        $this->db->where('users_id', $id);
        $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0) {
            $notif['message'] = 'Saved successfully';
            $notif['type'] = 'success';
            unset($_POST);
        } else {
            $notif['message'] = 'Something wrong !';
            $notif['type'] = 'danger';
        }
        return $notif;
    }

    public function ReiniciarSenha($id) {
        $notif = array();
       
        $data = array(
            'password' => Utils::hash('sha1','Tapajos123', AUTH_SALT),
        );
        $this->db->where('users_id', $id);
        $this->db->update('users', $data);
        if ($this->db->affected_rows() > 0) {
            $notif['message'] = 'Saved successfully';
            $notif['type'] = 'success';
            unset($_POST);
        } else {
            $notif['message'] = 'Something wrong !';
            $notif['type'] = 'danger';
        }
        return $notif;
    }

    /*
     * 
     */

    public function check_email($email) {
        $sql = "SELECT * FROM users WHERE email = " . $this->db->escape($email);
        $res = $this->db->query($sql);
        if ($res->num_rows() > 0) {
            $row = $res->row();
            return $row;
        }
        return null;
    }

    public function list_user(){
        $query = $this->db->query('SELECT users_id, username, first_name, last_name, email, is_active FROM users');
        return $query->result_array();
    }

    public function getUser($id) {
        $query = $this->db->query('SELECT users_id, username, first_name, last_name, email FROM users WHERE users_id ='.$id);   
        return $query->result_array();
    }

}
