<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Compras_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }


    public function RetornaClasses(){
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query('SELECT Ordem, Nome
                                    FROM Classes
                                    WHERE Ordem > 0
                                    ORDER BY Nome ASC');   
        return $query->result_array();
    }


    public function SugestaoProduto($prazo, $inicial, $final, $dias, $clase) {
        $resultado = null;
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT DISTINCT Prod_Serv.Ordem, Prod_Serv.Nome,(
                                        (SELECT SUM(ISNULL(Movimento_Prod_Serv.Quantidade,0))
                                        FROM Movimento_Prod_Serv
                                        JOIN Movimento ON Movimento_Prod_Serv.Ordem_Movimento = Movimento.Ordem
                                        WHERE Movimento_Prod_Serv.Linha_Excluida = 0
                                        AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                        AND Movimento_Prod_Serv.Ordem_Prod_Serv = Prod_Serv.Ordem
                                        AND Movimento.Data BETWEEN '".$inicial."' AND '".$final."'
                                        GROUP BY Movimento_Prod_Serv.Ordem_Prod_Serv
                                        )-(
                                        SELECT ISNULL(SUM(Movimento_Prod_Serv.Quantidade),0)
                                        FROM Movimento_Prod_Serv
                                        JOIN Movimento ON Movimento_Prod_Serv.Ordem_Movimento = Movimento.Ordem
                                        WHERE Movimento_Prod_Serv.Linha_Excluida = 0
                                        AND Ordem_Operacao = 30
                                        AND Movimento_Prod_Serv.Ordem_Prod_Serv = Prod_Serv.Ordem
                                        AND Movimento.Data BETWEEN '".$inicial."' AND '".$final."'        
                                        )) AS Vendas, 
                                        (
                                        ((SELECT SUM(ISNULL(Movimento_Prod_Serv.Quantidade,0))
                                        FROM Movimento_Prod_Serv
                                        JOIN Movimento ON Movimento_Prod_Serv.Ordem_Movimento = Movimento.Ordem
                                        WHERE Movimento_Prod_Serv.Linha_Excluida = 0
                                        AND (Ordem_Operacao = 124 OR Ordem_Operacao = 7)
                                        AND Movimento_Prod_Serv.Ordem_Prod_Serv = Prod_Serv.Ordem
                                        AND Movimento.Data BETWEEN '".$inicial."' AND '".$final."'
                                        GROUP BY Movimento_Prod_Serv.Ordem_Prod_Serv
                                        )-(
                                        SELECT ISNULL(SUM(Movimento_Prod_Serv.Quantidade),0)
                                        FROM Movimento_Prod_Serv
                                        JOIN Movimento ON Movimento_Prod_Serv.Ordem_Movimento = Movimento.Ordem
                                        WHERE Movimento_Prod_Serv.Linha_Excluida = 0
                                        AND Ordem_Operacao = 30
                                        AND Movimento_Prod_Serv.Ordem_Prod_Serv = Prod_Serv.Ordem
                                        AND Movimento.Data BETWEEN '".$inicial."' AND '".$final."'        
                                        ))/".$dias."
                                        ) AS Media,
                                        (SELECT TOP 1 (ISNULL(Estoque_Atual.Estoque_Ideal,0) * 0.5) + 2
                                        FROM Estoque_Atual
                                        WHERE Estoque_Atual.Ordem_Prod_Serv = Prod_Serv.Ordem 
                                        ORDER BY 1 DESC )	AS Minimo,
                                        (SELECT SUM(ISNULL(Estoque_Atual.Qtde_Estoque_Atual,0))
                                        FROM Estoque_Atual
                                        WHERE Estoque_Atual.Ordem_Prod_Serv = Prod_Serv.Ordem
                                        ) AS Atual	
                                    FROM Prod_Serv
                                    JOIN Estoque_Atual ON Prod_Serv.Ordem = Estoque_Atual.Ordem_Prod_Serv
                                    WHERE Qtde_Estoque_Atual <= (Estoque_Ideal * 0.5) + 2
                                    AND Pedidos_Compra = 0
                                    AND Ordem_Classe = ".$clase."
                                    ORDER BY Prod_Serv.Ordem ASC");   
        $datos = $query->result_array(); 

        $i = 0;
        $j = 0;
        while($i < count($datos)){
            $vendas = $datos[$i]['Vendas'];
            $media = $datos[$i]['Media']; 
            $minimo = $datos[$i]['Minimo'];
            $atual = $datos[$i]['Atual'];
            $sugestao = ($media * ($prazo + $dias)) - ($atual - $minimo);

            if($sugestao >= 1 && $vendas >= 1){
                $resultado[$j]['Ordem'] = $datos[$i]['Ordem'];
                $resultado[$j]['Nome'] =  $datos[$i]['Nome'];
                $resultado[$j]['Vendas'] = number_format ( $datos[$i]['Vendas'] ,  2 , "," ,  "." );
                $resultado[$j]['Media'] = number_format ( $datos[$i]['Media'] ,  4 , "," ,  "." );
                $resultado[$j]['Minimo'] = number_format ( $datos[$i]['Minimo'] , 2  , "," ,  "." );
                $resultado[$j]['Atual'] = number_format ( $datos[$i]['Atual'] , 2  , "," ,  "." );
                $resultado[$j]['Sugestao'] = number_format ( round($sugestao) , 2  , "," ,  "." );
                $j++;
            }

            $i++;
        }

        if ($resultado == null){
            $resultado = "";
        }
    
        return $resultado;
    }

    public function detalhesVendaProduto($codigo,$inicial,$final,$prazo,$dias){
        
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT a.Ordem_Filial AS FILIAL,SUM(ISNULL(Movimento_Prod_Serv.Quantidade,0)) - 
                                    (Select  ISNULL(SUM(Movimento_Prod_Serv.Quantidade),0) 
                                                FROM Movimento_Prod_Serv
                                                JOIN Movimento as b ON Movimento_Prod_Serv.Ordem_Movimento = b.Ordem
                                                JOIN PRod_SERV ON Prod_Serv.Ordem=Movimento_Prod_Serv.Ordem_Prod_Serv 
                                                WHERE 
                                                Movimento_Prod_Serv.Linha_Excluida = 0
                                                AND Ordem_Operacao = 30
                                                AND Movimento_Prod_Serv.Ordem_Prod_Serv = ".$codigo."
                                                AND b.Ordem_Filial=a.Ordem_Filial
                                                AND b.Data BETWEEN '".$inicial."' AND '".$final."'
                                                        ) AS VENDAS ,
                                        (
                                            SUM(ISNULL(Movimento_Prod_Serv.Quantidade,0)) - 
                                                        (Select  ISNULL(SUM(Movimento_Prod_Serv.Quantidade),0) 
                                                        FROM Movimento_Prod_Serv
                                                        JOIN Movimento as b ON Movimento_Prod_Serv.Ordem_Movimento = b.Ordem
                                                        JOIN PRod_SERV ON Prod_Serv.Ordem=Movimento_Prod_Serv.Ordem_Prod_Serv 
                                                        WHERE 
                                                        Movimento_Prod_Serv.Linha_Excluida = 0
                                                        AND Ordem_Operacao = 30
                                                        AND Movimento_Prod_Serv.Ordem_Prod_Serv = ".$codigo."
                                                        AND b.Ordem_Filial=a.Ordem_Filial
                                                        AND b.Data BETWEEN '".$inicial."' AND '".$final."'	 
                                                            ))/30 AS MEDIA, 
                                        (
                                                            SELECT TOP 1 (ISNULL(Estoque_Atual.Estoque_Ideal,0) * 0.5) + 2  FROM Estoque_Atual
                                                            WHERE Estoque_Atual.Ordem_Prod_Serv = ".$codigo."
                                                            ORDER BY 1 DESC 
                                                        ) AS MINIMO ,
                                                        (
                                                        SELECT SUM(ISNULL(Estoque_Atual.Qtde_Estoque_Atual,0))
                                                            FROM Estoque_Atual
                                                            WHERE Estoque_Atual.Ordem_Prod_Serv = ".$codigo." AND
                                                            Estoque_Atual.Ordem_Filial = a.Ordem_Filial 
                                                        
                                                        ) AS TOTAL
                                                            
                                                            FROM Movimento_Prod_Serv 
                                                            JOIN Movimento as a ON Movimento_Prod_Serv.Ordem_Movimento = a.Ordem
                                                            JOIN Prod_Serv ON Prod_Serv.Ordem = Movimento_Prod_Serv.Ordem_Prod_Serv
                                                            JOIN Estoque_Atual ON Prod_Serv.Ordem = Estoque_Atual.Ordem_Prod_Serv
                                                            WHERE 
                                                            Movimento_Prod_Serv.Linha_Excluida = 0
                                                            AND (a.Tipo_Operacao='VND')
                                                            AND Movimento_Prod_Serv.Ordem_Prod_Serv =".$codigo." AND a.Data  BETWEEN '".$inicial."' AND '".$final."' AND Estoque_Atual.Pedidos_Compra=0
                                                            GROUP BY Movimento_Prod_Serv.Ordem_Prod_Serv , a.Ordem_Filial");
                                                            $datos = $query->result_array();
                            
                            for($i=0; $i < count($datos); $i++){
                                $resultado[$i]['FILIAL'] = $datos[$i]['FILIAL'];
                                $resultado[$i]['VENDAS'] = $datos[$i]['VENDAS'];
                                $resultado[$i]['MINIMO'] = $datos[$i]['MINIMO'];
                                $resultado[$i]['MEDIA'] = $datos[$i]['MEDIA'];
                                $resultado[$i]['MINIMO'] = $datos[$i]['MINIMO'];   
                                $resultado[$i]['TOTAL']=$datos[$i]['TOTAL'];
                                      
                            }
                            //return $resultado;
                            $arrayTransfer[0]['FILIAL'] ='MATRIZ';
                            $arrayTransfer[1]['FILIAL']='EDUCANDOS';
                            $arrayTransfer[2]['FILIAL']='MULTIRÃO';
                            
                            
                            $somaMatriz=0.0;
                            $somaMultirao=0.0;
                            $somaEducandos=0.0;
                            $mediaMatriz=0.0;
                            $mediaEducandos=0.0;
                            $mediaMultirao=0.0;
                            $numMinimoMatriz=0;
                            $numMinimoMultirao=0;
                            $numMinimoEducandos=0;
                            $totalMatriz=0;
                            $totalEducandos=0;
                            $totalMultirao=0;

                            for ($i=0; $i < count($resultado); $i++){
                               
                                if( $resultado[$i]['FILIAL'] == 1 || $resultado[$i]['FILIAL'] == 10 || $resultado[$i]['FILIAL'] == 12){
                                    //MATRIZ
                                    $somaMatriz = $resultado[$i]['VENDAS']+$somaMatriz; 
                                    $mediaMatriz = $resultado[$i]['MEDIA'] + $mediaMatriz;
                                    $totalMatriz= $resultado[$i]['TOTAL'] + $totalMatriz;
                                    
                                }
                                else if($resultado[$i]['FILIAL'] == 3 || $resultado[$i]['FILIAL']==11){
                                   //EDUCANDOS
                                    $somaEducandos= $resultado[$i]['VENDAS']+$somaEducandos; 
                                    $mediaEducandos = $resultado[$i]['MEDIA'] + $mediaEducandos;
                                    $totalEducandos= $resultado[$i]['TOTAL'] + $totalEducandos;
      

                                }
                                else if($resultado[$i]['FILIAL'] ==4){
                                    //MULTIRÃO
                                    $somaMultirao = $resultado[$i]['VENDAS']+$somaMultirao; 
                                    $mediaMultirao = $resultado[$i]['MEDIA'] + $mediaMultirao;
                                    $totalMultirao= $resultado[$i]['TOTAL'] + $totalMultirao;
                                }   


                            }
                            $arrayTransfer[0]['VENDAS'] =number_format ( $somaMatriz,3,",",".");
                            $arrayTransfer[1]['VENDAS'] = number_format ( $somaEducandos,3,",",".");
                            $arrayTransfer[2]['VENDAS']=number_format ( $somaMultirao,3,",",".");
                            $arrayTransfer[0]['MEDIA']=number_format ( $mediaMatriz,3, "," ,  "." );
                            $arrayTransfer[1]['MEDIA']=number_format ( $mediaEducandos,3 , "," ,  "." );
                            $arrayTransfer[2]['MEDIA']=number_format ( $mediaMultirao,3, "," ,  "." );
                            $arrayTransfer[0]['MINIMO']=number_format($resultado[0]['MINIMO'],2,",",".");
                            $arrayTransfer[1]['MINIMO']=number_format($resultado[0]['MINIMO'],2,",",".");
                            $arrayTransfer[2]['MINIMO']=number_format($resultado[0]['MINIMO'],2,",",".");
                            $arrayTransfer[0]['ATUAL']=number_format ( $totalMatriz,2, "," ,  "." );
                            $arrayTransfer[1]['ATUAL']=number_format ( $totalEducandos,2 , "," ,  "." );
                            $arrayTransfer[2]['ATUAL']=number_format ( $totalMultirao,2, "," ,  "." );
                            
                            for($i=0; $i < count($arrayTransfer); $i++){

                                
                                $arrayTransfer[$i]['SUGESTAO'] = ($arrayTransfer[$i]['MEDIA'] * ($prazo + $dias)) - ($arrayTransfer[$i]['ATUAL']- $arrayTransfer[$i]['MINIMO']);
                                $arrayTransfer[$i]['SUGESTAO'] = number_format ( $arrayTransfer[$i]['SUGESTAO'],1, "," ,  "." );
                            }
                           
                            return ($arrayTransfer);

    } 

    public function detalhesCompras($codigo){
                $sqlserver = $this->load->database('sqlserver', TRUE);
                $query = $sqlserver->query('SELECT TOP 5  Movimento.Ordem_Filial as FILIAL,Movimento.Sequencia "CODPEDIDO",Cli_For.ORDEM as "FORNECEDOR",
                                            Movimento_Prod_Serv.Preco_Unitario AS UNIDADE,Movimento_Prod_Serv.Preco_Total_Sem_Desconto as TOTAL,Movimento_Prod_Serv.Quantidade_Itens AS "QTDITENS" ,MONTH(Movimento.data) as MES
                                            FROM Movimento 
                                                    INNER JOIN Movimento_Prod_Serv ON Movimento_Prod_Serv.Ordem_Movimento = Movimento.Ordem
                                                    INNER JOIN Prod_Serv ON  Prod_Serv.Ordem = Movimento_Prod_Serv.Ordem_Prod_Serv
                                                    INNER JOIN Cli_For On Cli_For.Ordem = Movimento.Ordem_Cli_For
                                                    Inner JOIN Filiais On Filiais.Ordem = Movimento.Ordem_Filial
                                            
                                            WHERE 
                                                    (Movimento.Tipo_Operacao=\'COM\') AND 
                                                    (Movimento.Ordem_Filial=1 OR Movimento.Ordem_Filial=10 OR Movimento.Ordem_Filial=12) AND 
                                                    (Efetivado_Financeiro = 1 AND Desefetivado_Financeiro = 0 ) and 
                                                    Movimento_Prod_Serv.Linha_Excluida=0 AND
                                                    Movimento_Prod_Serv.Ordem_Prod_Serv='.$codigo.' AND 
                                                    YEAR(Movimento."Data")=YEAR(getdate())
                                            ORDER BY 
                                                    Month (Movimento."Data")');
                    $datos = $query->result_array();
                    
                    for($i=0; $i < count($datos); $i++){
                        $resultado[$i]['FILIAL'] = $datos[$i]['FILIAL'];
                        $resultado[$i]['CODPEDIDO']=$datos[$i]['CODPEDIDO'];
                        $resultado[$i]['FORNECEDOR'] = $datos[$i]['FORNECEDOR'];
                        $resultado[$i]['UNIDADE'] = number_format($datos[$i]['UNIDADE'],2,",",".");//number_format($resultado[0]['MINIMO'],2,",",".");
                        $resultado[$i]['TOTAL'] = number_format($datos[$i]['TOTAL'],2,",",".");
                        $resultado[$i]['QTDITENS'] =number_format($datos[$i]['QTDITENS'],2,",",".");
                        $resultado[$i]['MES'] = $datos[$i]['MES'];
                    }


                    return $resultado;




    } 

    public function detalhesVendas($codigo){
        $sqlserver = $this->load->database('sqlserver', TRUE);
        $query = $sqlserver->query("SELECT M.Ordem_Filial AS FILIAL,ISNULL(Sum((MP.Quantidade)),0) as VENDAS,
                                    (SELECT EA.ABC from Estoque_Atual as EA
                                    Inner Join Prod_Serv  as PS2 on PS2.Ordem=EA.Ordem_Prod_Serv
                                            where 
                                            PS2.Ordem=".$codigo." And
                                            EA.Ordem_Filial = M.Ordem_Filial
                                            ) AS ABC,	
                                        (SELECT ISNULL(SUM((MP1.Quantidade)),0)
                                From
                                    Movimento as M1
                                    Inner Join
                                        Movimento_Prod_Serv as MP1 On MP1.Ordem_Movimento = M1.Ordem
                                        Inner Join 
                                        Prod_Serv  as PS1 On PS1.Ordem = MP1.Ordem_Prod_Serv
                                        where
                                        (M1.Tipo_OPeracao = 'DEV') And
                                        (M1.Desefetivado_Financeiro = 1) And
                                        (M1.Desefetivado_Estoque = 1) And
                                        PS1.Ordem=".$codigo." And
                                        M1.Ordem_Filial = M.Ordem_Filial
                                    ) AS DEVOLUCOES
                                    
                                From
                                    Movimento as M
                                    Inner Join
                                    Movimento_Prod_Serv as MP On MP.Ordem_Movimento = M.Ordem
                                    Inner Join
                                    Prod_Serv  as PS On PS.Ordem = MP.Ordem_Prod_Serv
                                    Inner Join Operacoes as OP  On OP.Ordem=M.Ordem_Operacao
                                    Where
                                        (M.Efetivado_Financeiro = 1) And
                                        (M.Desefetivado_Financeiro = 0) And
                                        (M.Efetivado_Estoque = 1) And
                                        (M.Desefetivado_Estoque = 0) And
                                        (MP.Linha_Excluida = 0) And
                                        OP.entrada_saida = 'S' And 
                                        OP.tipo = 'VND' And 
                                        OP.movimenta_estoque = 1 And 
                                        OP.movimenta_dinheiro = 1 And 
                                        OP.movimenta_comissao = 1 And
                                        PS.Ordem=".$codigo."
                                    Group By
                                        M.Ordem_Filial,
                                        PS.Ordem");
       $datos = $query->result_array();
        
       
       for($i=0; $i < count($datos); $i++){
                        $resultado[$i]['FILIAL'] = $datos[$i]['FILIAL'];
                        $resultado[$i]['VENDAS'] = number_format($datos[$i]['VENDAS'],2,",",".");
                        $resultado[$i]['ABC'] = $datos[$i]['ABC']; 
                        $resultado[$i]['DEVOLUCOES'] = number_format($datos[$i]['DEVOLUCOES'],2,",",".");

                    } 

       return $resultado;
        }
}