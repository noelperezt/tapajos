<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class General_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function utf8_converter($array){
        
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
                $item = utf8_encode($item);
        }
    });
 
    return $array;
}

}