<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {

    var $session_user;

    function __construct() {
        parent::__construct();

        Utils::no_cache();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('auth/login'));
            exit;
        }
        $this->session_user = $this->session->userdata('logged_in');
    }

    /*
     * 
     */

    public function index() {
        
    }

    public function SugestaoDeCompra() {
        $data['title'] = 'Dashboard';
        $data['session_user'] = $this->session_user;

        $this->load->model('compras_model');
        $dados['datos'] = $this->compras_model->RetornaClasses();

        $this->load->view('plantilla/header', $data);
        $this->load->view('plantilla/menu');
        $this->load->view('compras/sugestaocompra', $dados);
        $this->load->view('plantilla/footer');
    }

    public function SugestaoProduto($prazo, $inicio, $fin, $dias, $clase){
        if($this->input->is_ajax_request()){
            $this->load->model('compras_model');
            $datos = $this->compras_model->SugestaoProduto($prazo, $inicio, $fin, $dias, $clase);
            echo json_encode($datos);
        }else{
            show_404();
        }

    }
    
    public function detalheProduto($codigo,$inicial,$final,$prazo,$dias){

       if($this->input->is_ajax_request()){
            $this->load->model('compras_model');
            $datos = $this->compras_model->detalhesVendaProduto($codigo,$inicial,$final,$prazo,$dias);
            echo json_encode($datos);
        }else{
            show_404();
        } 
    }

    public function detalhesCompras($codigo){

        if($this->input->is_ajax_request()){
            $this->load->model('compras_model');
            $datos = $this->compras_model->detalhesCompras($codigo);
             echo json_encode($datos);
         }else{
             show_404();
         } 
     }
    

    

     public function detalhesVendas($codigo){

        if($this->input->is_ajax_request()){
            $this->load->model('compras_model');
            $datos = $this->compras_model->detalhesVendas($codigo);
             echo json_encode($datos);
         }else{
             show_404();
         } 
     }
}