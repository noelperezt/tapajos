<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends CI_Controller {

    var $session_user;

    function __construct() {
        parent::__construct();

        Utils::no_cache();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('auth/login'));
            exit;
        }
        $this->session_user = $this->session->userdata('logged_in');
        
    }

    /*
     * 
     */

    public function index() {

        $data['title'] = 'Dashboard';
        $data['session_user'] = $this->session_user;

        $this->load->model('Stock_model');
        $this->load->model('General_model');
        $dados['filiais']= $this->General_model->utf8_converter($this->Stock_model->ListarFiliais());

        /*
         * Load view
         */
        $this->load->view('plantilla/header', $data);
        $this->load->view('plantilla/menu');
        $this->load->view('stock/lista',$dados);
        $this->load->view('plantilla/footer');
    }

    public function ListaStock($filial){
        if($this->input->is_ajax_request()){
            $this->load->model('stock_model');
            $this->load->model('General_model');
            $datos = $this->stock_model->ListarStock($filial);
            echo json_encode($this->General_model->utf8_converter($datos));
        }else{
            show_404();
        }
    }

}
