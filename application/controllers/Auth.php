<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();

        Utils::no_cache();
       /* if ($this->session->userdata('logged_in')) {
            redirect(base_url('dashboard'));
            exit;
        }*/
    }

    /**
     *     Sara1984      
     */
    public function index() {
        redirect(base_url('auth/login'));
    }

    /**
     *           
     */
    public function login() {
        $data['title'] = 'Login';
        $this->load->model('auth_model');

        if (count($_POST)) {
            $this->load->helper('security');
            //$this->form_validation->set_rules('email', 'Email address', 'trim|required|valid_email|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

            if ($this->form_validation->run() == false) {
                $data['notif']['message'] = validation_errors();
                $data['notif']['type'] = 'danger';
            } 
            else {
                $data['notif'] = $this->auth_model->Authentification();
            }
        }

        if ($this->session->userdata('logged_in')) {
            redirect(base_url('dashboard'));
            exit;
        }

        /*
         * Load view
         */
        $this->load->view('auth/login');
    }

    /**
     *           
     */
    public function register() {
        $this->load->model('auth_model');

        if($this->input->is_ajax_request()){
            $this->load->helper('security');

            $this->form_validation->set_rules('user_name', 'Usuario', 'trim|required|is_unique[users.username]');
            $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
            $this->form_validation->set_rules('sobrenome', 'Sobrenome', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');

            $this->form_validation->set_rules('password', 'Password', 'trim|required');
            $this->form_validation->set_rules('confirm', 'Password', 'trim|required|matches[password]|min_length[6]|alpha_numeric|callback_password_check');
            
            $this->form_validation->set_message('is_unique', '<strong>%s</strong> já registrado');
            $this->form_validation->set_message('required', '<strong>%s</strong> obrigatório');
            $this->form_validation->set_message('valid_email', '<strong>%s</strong> não é válido');
            $this->form_validation->set_message('password_check', 'A senha deve conter Letra Maiuscula, minuscula e número');
            $this->form_validation->set_message('matches', 'O campo <strong>Senha</strong> e <strong>Confirmação</strong> não estão iguais');

            if ($this->form_validation->run() == false) {
                $data['notif']['message'] = validation_errors();
                $data['notif']['type'] = 'danger';
                echo json_encode ($data);
            } 
            else {
                $data['notif'] = $this->auth_model->register();
                echo json_encode ($data);
            }
        }
    }

    public function update() {
        $this->load->model('auth_model');

        if($this->input->is_ajax_request()){
            $this->load->helper('security');

            $this->form_validation->set_rules('user_name', 'Usuario', 'trim|required|is_unique[users.username]');
            $this->form_validation->set_rules('nome', 'Nome', 'trim|required');
            $this->form_validation->set_rules('sobrenome', 'Sobrenome', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
            
            $this->form_validation->set_message('is_unique', '<strong>%s</strong> já registrado');
            $this->form_validation->set_message('required', '<strong>%s</strong> obrigatório');
            $this->form_validation->set_message('valid_email', '<strong>%s</strong> não é válido');

            if ($this->form_validation->run() == false) {
                $data['notif']['message'] = validation_errors();
                $data['notif']['type'] = 'danger';
                echo json_encode ($data);
            } 
            else {
                $data['notif'] = $this->auth_model->update();
                echo json_encode ($data);
            }
        }
    }

    public function getUsuario($id) {
        $this->load->model('auth_model');

        if($this->input->is_ajax_request()){
            $data = $this->auth_model->getUser($id);
            echo json_encode($data);
        }
    }

    /**
     *           
     */
    public function forgot_password() {
        $data['title'] = 'Forgot password';
        $this->load->model('auth_model');

        if (count($_POST)) {
            $this->load->helper('security');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            
            if ($this->form_validation->run() == false) {
                $data['notif']['message'] = validation_errors();
                $data['notif']['type'] = 'danger';
            } 
            else {
                $result = $this->auth_model->check_email($this->input->post('email'));
                if($result){
                    $data['notif']['message'] = 'Implement the function to send the email';
                    $data['notif']['type'] = 'success';
                }
                else{
                    $data['notif']['message'] = 'This email does not exist on the system';
                    $data['notif']['type'] = 'danger';
                }
            }
        }

        /*
         * Load view
         */
        $this->load->view('auth/includes/header', $data);
        $this->load->view('auth/includes/navbar');
        $this->load->view('auth/forgot_password');
        $this->load->view('auth/includes/footer');
    }

    /*
     * Custom callback function
     */

    public function password_check($str) {
        if (preg_match('#[0-9]#', $str) && preg_match('#[a-zA-Z]#', $str)) {
            return true;
        }
        return false;
    }
    
    /*
     * 
     */

    public function logout() {
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
        $this->output->set_header("Pragma: no-cache");
        redirect(base_url('auth/login'));
    }

    public function CambiarStatus($id){
        $this->load->model('auth_model');
        if($this->input->is_ajax_request()){
		    $respuesta = $this->auth_model->CambiarStatus($id);
            echo json_encode($respuesta);
        }
    }
    
    public function ReiniciarSenha($id){
        $this->load->model('auth_model');
        if($this->input->is_ajax_request()){
		    $respuesta = $this->auth_model->ReiniciarSenha($id);
            echo json_encode($respuesta);
        }
	}

}
