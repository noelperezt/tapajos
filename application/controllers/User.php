<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    var $session_user;

    function __construct() {
        parent::__construct();

        Utils::no_cache();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('auth/login'));
            exit;
        }
        $this->session_user = $this->session->userdata('logged_in');
    }

    /*
     * 
     */

    public function index() {
        
    }

    public function ListUsers() {
        $data['title'] = 'Dashboard';
        $data['session_user'] = $this->session_user;

        $this->load->view('plantilla/header', $data);
        $this->load->view('plantilla/menu');
        $this->load->view('usuarios/usuarios');
        $this->load->view('plantilla/footer');
    }

    public function CargarUsuarios(){
        if($this->input->is_ajax_request()){
            $this->load->model('auth_model');
            $datos = $this->auth_model->list_user();
            echo json_encode($datos);
        }else{
            show_404();
        }
    }
    
    
}
