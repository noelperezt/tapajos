<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    var $session_user;

    function __construct() {
        parent::__construct();

        Utils::no_cache();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('auth/login'));
            exit;
        }
        $this->session_user = $this->session->userdata('logged_in');
        
    }

    /*
     * 
     */

    public function index() {

        $data['title'] = 'Dashboard';
        $data['session_user'] = $this->session_user;

        $this->load->model('Graficas');
        $dados['resultados']= $this->Graficas->TopVendedor();
        $dados['ventas_ano']= $this->Graficas->VentasAno();
        $dados['cont_ventas_ano']= $this->Graficas->ContVentasAno();
        $dados['ventas_mes']= $this->Graficas->VentasMes();
        $dados['cont_ventas_mes']= $this->Graficas->ContVentasMes();
        $dados['contas_pagar']= $this->Graficas->ContasPagar();
        $dados['cont_contas_pagar']= $this->Graficas->ContContasPagar();
        $dados['contas_receber']= $this->Graficas->ContasReceber();
        $dados['cont_contas_receber']= $this->Graficas->ContContasReceber();

        $this->load->model('General_model');

        $this->load->view('plantilla/header', $data);
        $this->load->view('plantilla/menu');
        $this->load->view('inicio', $this->General_model->utf8_converter($dados));
        $this->load->view('plantilla/footer');
    }

    public function VentasPorAno($ano){
        if($this->input->is_ajax_request()){
            $this->load->model('Graficas');
            $this->load->model('General_model');
            $datos = $this->Graficas->TotalPorAno($ano);
            echo json_encode($this->General_model->utf8_converter($datos));
        }else{
            show_404();
        }
    }

}
