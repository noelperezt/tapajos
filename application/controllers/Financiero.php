<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Financiero extends CI_Controller {

    var $session_user;

    function __construct() {
        parent::__construct();

        Utils::no_cache();
        if (!$this->session->userdata('logged_in')) {
            redirect(base_url('auth/login'));
            exit;
        }
        $this->session_user = $this->session->userdata('logged_in');
    }

    /*
     * 
     */

    public function index() {
        
    }

    public function DuplicatasTransportadora() {
        $data['title'] = 'Dashboard';
        $data['session_user'] = $this->session_user;

        $this->load->model('financiero_model');
        $datos['resultados'] = $this->financiero_model->DuplicatasTransportadora();
        $datos['vencidos'] = $this->financiero_model->CantTotalVencidas();
        $datos['mes'] = $this->financiero_model->CantVenceMes();
        $datos['semana'] = $this->financiero_model->CantVenceSemana();
        $datos['dia'] = $this->financiero_model->CantVenceDia();
        $datos['total_vencidos'] = $this->financiero_model->TotalVencidas();
        $datos['total_mes'] = $this->financiero_model->VenceMes();
        $datos['total_semana'] = $this->financiero_model->VenceSemana();
        $datos['total_dia'] = $this->financiero_model->VenceDia();

        $this->load->view('plantilla/header', $data);
        $this->load->view('plantilla/menu');
        $this->load->view('financiero/duplicatastransportadora', $datos);
        $this->load->view('plantilla/footer');
    }

    public function CargarDuplicatas(){
        if($this->input->is_ajax_request()){
            $this->load->model('financiero_model');
            $datos = $this->financiero_model->DuplicatasTransportadora();
            echo json_encode($datos);
        }else{
            show_404();
        }
    }

    public function ConsultaRelatorioPorData($inicio, $fin){
        if($this->input->is_ajax_request()){
            $this->load->model('financiero_model');
            $this->load->model('General_model');
            $datos = $this->financiero_model->RelatorioData($inicio,$fin);
            echo json_encode($this->General_model->utf8_converter($datos));
        }else{
            show_404();
        }
    }

    public function ConsultaRelatorioPorHora($inicio, $fin){
        if($this->input->is_ajax_request()){
            $this->load->model('financiero_model');
            $this->load->model('General_model');
            $datos = $this->financiero_model->RelatorioHora($inicio,$fin);
            echo json_encode($this->General_model->utf8_converter($datos));
        }else{
            show_404();
        }
    }

    public function RelatorioPorData() {
        $data['title'] = 'Dashboard';
        $data['session_user'] = $this->session_user;

        $this->load->view('plantilla/header', $data);
        $this->load->view('plantilla/menu');
        $this->load->view('financiero/relatoriodata');
        $this->load->view('plantilla/footer');
    }

    public function RelatorioPorHora() {
        $data['title'] = 'Dashboard';
        $data['session_user'] = $this->session_user;

        $this->load->view('plantilla/header', $data);
        $this->load->view('plantilla/menu');
        $this->load->view('financiero/relatoriohora');
        $this->load->view('plantilla/footer');
    }
    
    
}
