<section class="content">
        <div  class="container-fluid">

            <div class="body">
              <ol class="breadcrumb breadcrumb-bg-white breadcrumb-col-red align-right">
                  <li class = "active"><a style = "color: #777 !important;" href="javascript:void(0);"><i class="material-icons">person</i> Usuarios</a></li>
              </ol>          
            </div>

            <div  class="row clearfix">
                <!-- Line Chart -->
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id = "data" class="card">
                        <div class="header">
                            <div class = "row">
                                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                    <h2 style= "margin-top: 7px;">Usuarios</h2>
                                </div>
                            </div> 
                        </div>
                        <div class="body">

                            <div class="table-responsive">
                            <button title = "Cadastrar Novo" style="font-size: 12px; margin-bottom:20px;" data-toggle="modal" onclick="add_user()" class="btn btn-success" onclick="edit_book()">Novo Cadastro</button>
                                <table id="usuarios" class="table table-bordered table-striped table-hover js-basic-example dataTable" >
                                <thead>
                                  <tr>
                                      <th>Id</th>
                                      <th>User Name</th>
                                      <th>Nome</th>
                                      <th>Sobrenome</th>
                                      <th>Email</th>
                                      <th>Estado</th>
                                      <th>Ação </th>
                                  </tr>
                                </thead>
                                <tdody>
                                </tbody>    
                                </table>
                            </div>

                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Line Chart -->
            </div>
        </div>
    </section>              


        <!-- Modal -->
<div class="modal fade" id="modal_form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Usuarios</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id= "form" class="form-horizontal form-label-left">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome de Usuario</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name= "user_name" type="text" class="form-control" >
              </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name= "nome" type="text" class="form-control" >
              </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sobrenome</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name= "sobrenome" type="text" class="form-control" >
              </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name= "email" type="email" class="form-control" >
              </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Senha</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name="password" type="password" class="form-control" >
              </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirmação</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name= "confirm" type="password" class="form-control" >
              </div>
            </div>           
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" onclick="save()">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modal_form_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Usuarios</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id= "form_editar" class="form-horizontal form-label-left">
          <input name= "id_user" type="hidden">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome de Usuario</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name= "user_name" type="text" class="form-control" >
              </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name= "nome" type="text" class="form-control" >
              </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Sobrenome</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name= "sobrenome" type="text" class="form-control" >
              </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
              <div class="col-md-9 col-sm-9 col-xs-12">
                  <input name= "email" type="email" class="form-control" >
              </div>
            </div>         
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" onclick="save()">Guardar</button>
      </div>
    </div>
  </div>
</div>


 <!-- Datatables -->
   <!-- Jquery DataTable Plugin Js -->
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/datatables/keytable/js/dataTables.keyTable.js"></script>
    <script src="<?php echo base_url(); ?>build/js/financiero.js"></script>

    <script type="text/javascript" charset="utf8">
    $(document).ready(function() {
      var url_base = '<?php echo base_url();?>';
      table = $('#usuarios').DataTable({
          "language": {
                "search": "Pesquisar",
                "lengthMenu": "Mostrando  _MENU_ registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Mostrando página 0 de 0",
                "infoFiltered": "(Filtrado de _MAX_ entradas)",
                "loadingRecords": "Carregando...",
                "processing": "Processando...",
                "zeroRecords": "Nenhum resultado encontrado",
                "paginate": {
                    "first":"Primeiro",
                    "previous":"Anterior",
                    "next": "Seguinte",
                    "last": "Ultimo"
                }
            },
            "ajax": {
              "url": url_base + "user/CargarUsuarios",
              "type": "POST",
              "dataSrc": ""
            },
            "columns": [
              {"data": "users_id"},
              {"data": "username"},
              {"data": "first_name"},
              {"data": "last_name"},
              {"data": "email"},
              {"data": null, 
                "render": function ( data, type, row ) {
                    if (data.is_active == 1){
                      return '<span class="label label-success">Ativo</span>';
                    }else{
                      return '<span class="label label-danger">Inativo</span>'; 
                    }
                },},
              {"data": null, 
                "render": function ( data, type, row ) {
                      return '<button data-toggle="tooltip" data-placement="top" title = "Editar" style="font-size: 12px ; padding: 0px 3px;" class="btn btn-warning" onclick="edit_user('+data.users_id+')"><i class="glyphicon glyphicon-pencil"></i></button><button data-toggle="tooltip" data-placement="top" title = "Mudar Status" style="font-size: 12px ; padding: 0px 3px;" class="btn btn-danger" onclick="cambiarstatus('+data.users_id+')"><i class="glyphicon glyphicon-remove"></i></button><button data-toggle="tooltip" data-placement="top" title = "Reiniciar Senha" style="font-size: 12px ; padding: 0px 3px;" class="btn btn-info" onclick="reiniciarsenha('+data.users_id+')"><i class="glyphicon glyphicon-wrench"></i></button>';
                },}
            ],
            "select": {
            "style":    'os',
            "blurable": true
        },
            "order": [[ 0, "asc" ]],
            "select": true,
        "buttons": [
            {
                "extend": 'collection',
                "text": 'Export',
                "buttons": [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ]
        });
       
    } );
    </script>

    <?php
        require_once('build/ajax/usuarios.php');
    ?>

   