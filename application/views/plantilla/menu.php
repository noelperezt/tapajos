<!-- Menu -->
<div id ="sidebar-menu" class="menu">
                <ul class="list">
                    <li class="header">OPÇÕES DE NAVEGAÇÃO</li>
                    <li >
                        <a href="<?php echo base_url()?>dashboard">
                            <i class="material-icons">home</i>
                            <span>Principal</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">local_atm</i>
                            <span>Financeiro</span>
                        </a>
                        <ul class="ml-menu child_menu">
                            <li>
                                <a href="<?php echo base_url()?>financeiro/duplicatastransportadora/">
                                    <span>Fretes a Pagar</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url()?>financeiro/relatoriopordata/">
                                    <span>Vendas por Período Data</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url()?>financeiro/relatorioporhora/">
                                    <span>Vendas por Período Hora</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">domain</i>
                            <span>Comercial</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">shopping_basket</i>
                            <span>Produtos</span>
                        </a>
                        <ul class="ml-menu child_menu">
                            <li>
                                <a href="<?php echo base_url()?>estoque/">
                                    <span>Pocisão de Estoque</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">shopping_cart</i>
                            <span>Compras</span>
                        </a>
                        <ul class="ml-menu child_menu">
                            <li>
                                <a href="<?php echo base_url()?>compras/SugestaoDeCompra">
                                    <span>Sugestão de Estoque</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo base_url()?>user/listusers/">
                            <i class="material-icons">person</i>
                            <span>Usuarios</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2018 Desenvolvido por <a target = "_blank" href="http://result.inf.br">Result Solutions</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.0
                </div>
            </div>
            <!-- #Footer -->
        </aside>
    </section>