<!-- Select Plugin Js -->
<!--<script src="<?php echo base_url(); ?>build/plugins/bootstrap-select/js/bootstrap-select.js"></script>-->

<!-- Slimscroll Plugin Js -->
<script src="<?php echo base_url(); ?>build/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?php echo base_url(); ?>build/plugins/node-waves/waves.js"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="<?php echo base_url(); ?>build/plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Morris Plugin Js -->
<script src="<?php echo base_url(); ?>build/plugins/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>build/plugins/morrisjs/morris.js"></script>

<!-- ChartJs -->
<script src="<?php echo base_url(); ?>build/plugins/chart.js/Chart.bundle.js"></script>

<!-- Flot Charts Plugin Js -->
<script src="<?php echo base_url(); ?>build/plugins/flot-charts/jquery.flot.js"></script>
<script src="<?php echo base_url(); ?>build/plugins/flot-charts/jquery.flot.resize.js"></script>
<script src="<?php echo base_url(); ?>build/plugins/flot-charts/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(); ?>build/plugins/flot-charts/jquery.flot.categories.js"></script>
<script src="<?php echo base_url(); ?>build/plugins/flot-charts/jquery.flot.time.js"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="<?php echo base_url(); ?>build/plugins/jquery-sparkline/jquery.sparkline.js"></script>

<!-- Input Mask Plugin Js -->
<script src="<?php echo base_url(); ?>build/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

<!-- Custom Js -->
<script src="<?php echo base_url(); ?>build/js/admin.js"></script>


<!-- Demo Js -->
<script src="<?php echo base_url(); ?>build/js/demo.js"></script>

<script src="<?php echo base_url(); ?>build/js/custom.js"></script>

<script src="<?php echo base_url(); ?>build/plugins/jquery-sparkline/jquery.sparkline.js"></script>

<script src="<?php echo base_url(); ?>build/js/pages/widgets/infobox/infobox-2.js"></script>
<script src="<?php echo base_url(); ?>build/plugins/jquery-countto/jquery.countTo.js"></script>
<script src="<?php echo base_url(); ?>build/plugins/moment/moment.js"></script>
<script src="<?php echo base_url(); ?>build/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>build/plugins/waitme/waitme.js"></script>
<script src="<?php echo base_url(); ?>build/js/pages/ui/tooltips-popovers.js"></script>

</body>

</html>

