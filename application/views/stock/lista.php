<section class="content">
        <div  class="container-fluid">

            <div class="body">
              <ol class="breadcrumb breadcrumb-bg-white breadcrumb-col-red align-right">
                  <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i> Produtos</a></li>
                  <li class = "active"><a style = "color: #777 !important;" href="javascript:void(0);"> Pocisão de Estoque</a></li>
              </ol>          
            </div>

            <div  class="row clearfix">
                <!-- Line Chart -->
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id = "data" class="card">
                        <div class="header">
                            <div class = "row">
                                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                    <h2 style= "margin-top: 7px;">Pocisão de Estoque</h2>
                                </div>
                            </div> 
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                    <?php foreach($filiais as $resultado):?>
                                      <li><a onclick= "ListarStock(<?php echo $resultado['Ordem']; ?>)"><?php echo $resultado['Nome']; ?></a></li>
                                    <?php endforeach?>
                                    </ul>
                                </li>
                            </ul> 
                        </div>
                        <div class="body">

                            <div class="table-responsive">
                                <table id="stock" class="table table-bordered table-striped table-hover js-basic-example dataTable" style = "font-size: 11px">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Descrição</th>
                                        <th>Qtde Mínima</th>
                                        <th>Qtde Atual</th>
                                        <th>Qtde Ideal</th>
                                    </tr>
                                  </thead>
                                  <tdody>
                                  </tbody>
                                </table>
                            </div>

                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Line Chart -->
            </div>
        </div>
    </section>              
       

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

 <!-- Datatables -->
   <!-- Jquery DataTable Plugin Js -->
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/datatables/keytable/js/dataTables.keyTable.js"></script>
    <script src="<?php echo base_url(); ?>build/js/financiero.js"></script>

    <script type="text/javascript" charset="utf8">
    $(document).ready(function() {
      var url_base = '<?php echo base_url();?>';
        $('#stock').DataTable({
          "language": {
              "search": "Pesquisar",
              "lengthMenu": "Mostrando  _MENU_ registros",
              "info": "Mostrando página _PAGE_ de _PAGES_",
              "infoEmpty": "Mostrando página 0 de 0",
              "infoFiltered": "(Filtrado de _MAX_ entradas)",
              "loadingRecords": "Carregando...",
              "processing": "Processando...",
              "zeroRecords": "Nenhum resultado encontrado",
              "decimal": ",",
              "thousands": ".",
              "paginate": {
                  "first":"Primeiro",
                  "previous":"Anterior",
                  "next": "Seguinte",
                  "last": "Ultimo"
                    }   
               },
               "keys": true,
            "select": true,
        "buttons": [
            {
                "extend": 'collection',
                "text": 'Export',
                "buttons": [
                    'copy',
                    'excel',
                    'csv',
                    'pdf',
                    'print'
                ]
            }
        ]
        });
       
    } );
    </script>

    <?php
        require_once('build/ajax/stock.php');
    ?>


   