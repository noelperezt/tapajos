
<section class="content">
        <div  class="container-fluid">
            <div class="body">
              <ol class="breadcrumb breadcrumb-bg-white breadcrumb-col-red align-right">
                  <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i> Compras</a></li>
                  <li class = "active"><a style = "color: #777 !important;" href="javascript:void(0);"> Sugestão de Estoque</a></li>
              </ol>          
            </div>

            <div  class="row clearfix">
                <!-- Line Chart -->
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id = "data" class="card">
                        <div class="header">
                            <div class = "row">
                                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                    <h2 style= "margin-top: 7px;">Sugestão de Estoque</h2>
                                </div>
                            </div>  
                        </div>
                        <div class="body">
                            <div class = "row clearfix">
                                <div class="col-lg-12 col-md-8 col-sm-6 col-xs-12">
                                    <select id = "clase" class="form-control show-tick" data-show-subtext="true" data-live-search="true">
                                        <option value = "">--Classe--</option>
                                        <?php 
                                            foreach ($datos as $resultado):?> 
                                                <option value = "<?php echo $resultado['Ordem']; ?>"> <?php echo strtoupper ($resultado['Nome']); ?></option>
                                      <?php endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class= "row clearfix demo-masked-input">
                                <div class="col-lg-4 col-md-8 col-sm-6 col-xs-12" style="margin-bottom:0px;">
                                <div class="form-group form-float">
                                        <div class="form-line">
                                            <input id = "previsao" type="text" class="form-control solo-numero" onkeyup="mostrarFechas();">
                                            <label class="form-label">Previsão de Compra</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-8 col-sm-6 col-xs-12" style="margin-bottom:0px;">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="inicio" class="form-control date" placeholder="Data Inicial" readonly="readonly">
                                            </div>
                                        </div>
                                </div>
                                <div class="col-lg-4 col-md-8 col-sm-6 col-xs-12" style="margin-bottom:0px;">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                            <div class="form-line">
                                                <input type="text" id="fin" class="form-control date" placeholder="Data Final" readonly="readonly">
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-8 col-sm-6 col-xs-12" style="margin-bottom:0px;">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input id= "entrega" type="text" class="form-control solo-numero">
                                            <label class="form-label">Prazo de entrega do Fornecedor</label>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-12 col-md-8 col-sm-6 col-xs-12">
                                    <div class="pull-right">
                                        <button id = "calcular" type="button" class="btn btn-success waves-effect">Calcular</button>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="table-responsive">
                                    <table id="sugestao_produto" class="table table-bordered table-striped table-hover js-basic-example dataTable" style = "font-size: 11px">
                                        <thead>
                                            <tr>
                                                <th>Ordem</th>
                                                <th>Nome</th>
                                                <th>Vendas</th>
                                                <th>Media</th>
                                                <th>Minimo</th>
                                                <th>Atual</th>
                                                <th>Sugestão</th>
                                            </tr>
                                        </thead>
                                        <tdody>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                            
                            
                    </div>
                </div>
            </div>
                <!-- #END# Line Chart -->
        </div>
    </section>        

    <!--Modal -->
    
    <!-- Large Size -->
    <div class="modal fade" id="Sugestao_id" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="largeModalLabel"> PRODUTO- COD:<span id="cod"><strong></strong></span> NOME:<span id="nome"><strong></strong></span></h4>
                </div>
                <div class="modal-body">
                    <!-- Basic Table -->
                      <div class="row">
                       <div class ="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="body table-responsive">
                                        
                                        <table class="table table-striped table-bordered table-hover" id="sugestao_detalhes">
                                            <caption><strong>SUGESTÃO DETALHADA POR FILIAL</strong></caption>    
                                            
                                        </table>
                                    </div>
                       </div> 
                       </div>
                       <div class = "row">
                       <div class = "col-lg-6 col-md-12 col-sm-12 col-xs-12">
                       <div class="body table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id ="detalhesVendas" >
                                        <caption><strong>VENDAS POR FILIAL</strong></caption> 
                                        </table>
                                    </div>
                       </div> 
                       <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                       <div class="body table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="detalhesCompras">
                                        <caption><strong>ÚLTIMAS COMPRAS</strong></caption>  
                                        </table>
                                    </div>
                       </div> 
                       </div>
                      </div>
                            
                        <!-- #END# Basic Table -->
                
                <div class="modal-footer">
                    
                    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Input Mask Plugin Js -->
    <script src="<?php echo base_url(); ?>build/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
  <!-- Datatables -->
   <!-- Jquery DataTable Plugin Js -->
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/datatables/keytable/js/dataTables.keyTable.js"></script>
    <script src="<?php echo base_url(); ?>build/js/financiero.js"></script>

    <script type="text/javascript" charset="utf8">

     $('.solo-numero').keyup(function (){
        this.value = (this.value + '').replace(/[^0-9]/g, '');
      });

    $( "#abrir" ).click(function() {
       
      //$('#Sugestao_id').modal('show');
      detalhesSugestao(198,'A','2016-05-01','2018-01-01',100,12);

    });

    function detalhesSugestao(codigo,nome,inicial,final,prazo,dias) {
        var url_base = '<?php echo base_url();?>'
        save_method = 'update';
        
        $.ajax({
        url : url_base+"/compras/detalheProduto" +"/"+ codigo+"/"+inicial+"/"+final+"/"+prazo+"/"+dias,
        type: "GET",
        dataType: "JSON",
        success: function(datos)
        {
            var d = null;
            d ='<thead>'+'<tr>'+
                    '<th>FILIAL</th>'+
                    '<th>VENDAS</th>'+
                    '<th>MÍNIMO</th>'+
                    '<th>ATUAL</th>'+
                    '<th>SUGESTÃO</th>'+
                    '</tr>'+
                    '</thead>';
            for (var i=0;i< datos.length; i++){
            d+= '<tr>'+
            '<td>'+datos[i].FILIAL+'</td>'+
            '<td>'+datos[i].VENDAS+'</td>'+
            '<td>'+datos[i].MINIMO+'</td>'+
            '<td>'+datos[i].ATUAL+'</td>'+
            '<td>'+datos[i].SUGESTAO+'</td>'+
            '</tr>';
            }   
            $("#sugestao_detalhes tr").remove();
            $("#sugestao_detalhes").append(d);
             
            //FUNCAO COMPRAS
            $("#cod").text(codigo);
            $("#nome").text(nome);
            detalhesCompras(codigo);
            //FUNCAO VENDAS
            detalhesVendas(codigo) 
            $('#Sugestao_id').modal('show');
            // show bootstrap modal when complete loaded
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            //alert('Error get data from ajax');
        }
    });
                  
    }

     function detalhesCompras(codigo) {
        var url_base = '<?php echo base_url();?>'
        save_method = 'update';
        
        $.ajax({
        url : url_base+"/compras/detalhesCompras/"+"/"+ codigo,
        type: "GET",
        dataType: "JSON",
        success: function(datos_compras)
        {
            var d_compras = null;
            d_compras ='<thead>'+'<tr>'+
                    '<th>FILIAL</th>'+
                    '<th>CODPEDIDO</th>'+
                    '<th>FORNECEDOR</th>'+
                    '<th>R$UNIDADE</th>'+
                    '<th>R$TOTAL</th>'+
                    '<th>QTDITENS</th>'+
                    '<th>MES</th>'+
                    '</tr>'+
                    '</thead>';
            for (var i=0;i< datos_compras.length; i++){
             d_compras+= '<tr>'+
            '<td>'+datos_compras[i].FILIAL+'</td>'+
            '<td>'+datos_compras[i].CODPEDIDO+'</td>'+
            '<td>'+datos_compras[i].FORNECEDOR+'</td>'+
            '<td>'+datos_compras[i].UNIDADE+'</td>'+
            '<td>'+datos_compras[i].TOTAL+'</td>'+
            '<td>'+datos_compras[i].QTDITENS+'</td>'+
            '<td>'+datos_compras[i].MES+'</td>'+
            '</tr>';
            }
            $("#detalhesCompras tr").remove();   
            $("#detalhesCompras").append(d_compras);
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
           // alert('Error get data from ajax');
        }
    });
                  
    }

       function detalhesVendas(codigo) {
        var url_base = '<?php echo base_url();?>'
        save_method = 'update';
        
        $.ajax({
        url : url_base+"/compras/detalhesVendas/"+"/"+ codigo,
        type: "GET",
        dataType: "JSON",
        success: function(datos_Vendas)
        {   var d_vendas = null;
            d_vendas ='<thead>'+'<tr>'+
                    '<th>FILIAL</th>'+
                    '<th>VENDAS</th>'+
                    '<th>ABC</th>'+
                    '<th>DEVOLUÇÕES</th>'+
                    '</tr>'+
                    '</thead>';
            for (var i=0;i< datos_Vendas.length; i++){
             d_vendas+= '<tr>'+
            '<td>'+datos_Vendas[i].FILIAL+'</td>'+
            '<td>'+datos_Vendas[i].VENDAS+'</td>'+
            '<td>'+datos_Vendas[i].ABC+'</td>'+
            '<td>'+datos_Vendas[i].DEVOLUCOES+'</td>'+
            '</tr>';
            }
            $("#detalhesVendas tr").remove();  
            $("#detalhesVendas").append(d_vendas);
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
           // alert('Error get data from ajax');
        }
    });
                  
    }
        

    $(document).ready(function() {

        $("#calcular").click(function () {
            cargar();
	    });
        var ahora = moment();
        var ayer = ahora.subtract(1,'days');

        console.log(ayer);

        var $demoMaskedInput = $('.demo-masked-input');
        //Date
        $demoMaskedInput.find('.date').inputmask('dd/mm/yyyy', { placeholder: '__/__/____' });

      var url_base = '<?php echo base_url();?>';
        $('#sugestao_produto').DataTable({
          responsive: true,
          "language": {
                "decimal": ",",
                "thousands": ".",
                "search": "Pesquisar",
                "lengthMenu": "Mostrando  _MENU_ registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Mostrando página 0 de 0",
                "infoFiltered": "(Filtrado de _MAX_ entradas)",
                "loadingRecords": "Carregando...",
                "processing": "Processando...",
                "zeroRecords": "Nenhum resultado encontrado",
                "paginate": {
                    "first":"Primeiro",
                    "previous":"Anterior",
                    "next": "Seguinte",
                    "last": "Ultimo"
                }
            },
            /*"ajax": {
              "url": url_base + "compras/SugestaoProduto",
              "type": "POST",
              "dataSrc": ""
            },*/
            "columns": [
              { 'width': '10%'},
              { 'width': '40%'},
              { 'width': '10%'},
              { 'width': '10%'},
              { 'width': '10%'},
              { 'width': '10%'},
              { 'width': '10%'}    
            ],
            "keys": true,
            "select": true
        });
       
    } );

    function Rayer(){
        var ahora = null;
        ahora = moment();
        var ayer = ahora.subtract(1,'days');

        return ayer;
    }

    function mostrarFechas(){
        var ahora = moment();
        var ayer = Rayer();
        var previsao = $('#previsao').val();
        var r = parseInt(previsao) + 1;
        var limite = ahora.subtract(r,'days');
        
        $('#inicio').val(ayer.format('DDMMYYYY'));
        $('#fin').val(limite.format('DDMMYYYY'));
        
     }

     function cargar(){
         var prazo = $('#entrega').val();
         var inicio = $('#inicio').val();
         var fin = $('#fin').val();
         var dias = $('#previsao').val();
         var clase = $('#clase').val();

         console.log(inicio);

         var inicial = inicio.split('/');
         var final = fin.split('/');
        
         var url_base = '<?php echo base_url();?>';
         var url = url_base + "compras/SugestaoProduto/"+prazo+"/"+final[2]+"-"+final[1]+"-"+final[0]+"/"+inicial[2]+"-"+inicial[1]+"-"+inicial[0]+"/"+dias+"/"+clase+"/"

        console.log(url);

       
        $('#sugestao_produto').DataTable({
          responsive: true,
          "language": {
                "decimal": ",",
                "thousands": ".",
                "search": "Pesquisar",
                "lengthMenu": "Mostrando  _MENU_ registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Mostrando página 0 de 0",
                "infoFiltered": "(Filtrado de _MAX_ entradas)",
                "loadingRecords": "Carregando...",
                "processing": "Processando...",
                "zeroRecords": "Nenhum resultado encontrado",
                "paginate": {
                    "first":"Primeiro",
                    "previous":"Anterior",
                    "next": "Seguinte",
                    "last": "Ultimo"
                }
            },
            "ajax": {
              "url": url,
              "type": "POST",
              "dataSrc": ""
            },
            "columns": [
              {"data": "Ordem", 'width': '10%'},
              {"data": null, 
                "render": function ( data, type, row ) {
                      return '<a href= "#" onclick="detalhesSugestao('+data.Ordem+',\''+data.Nome+'\',\''+final[2]+'-'+final[1]+'-'+final[0]+'\',\''+inicial[2]+'-'+inicial[1]+'-'+inicial[0]+'\','+prazo+','+dias+');return false;" >'+data.Nome+'</a>';
                },'width': '40%'},
              {"data": "Vendas", 'width': '10%'},
              {"data": "Media", 'width': '10%'},
              {"data": "Minimo", 'width': '10%'},
              {"data": "Atual", 'width': '10%'},
              {"data": "Sugestao", 'width': '10%'}    
            ],
            "keys": true,
            "order": [[ 6, "desc" ]],
            "destroy": true,
            "select": true
        });

        
     }
    </script>