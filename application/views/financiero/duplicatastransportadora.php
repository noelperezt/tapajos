<section class="content">
        <div  class="container-fluid">

            <div class="body">
              <ol class="breadcrumb breadcrumb-bg-white breadcrumb-col-red align-right">
                  <li><a href="javascript:void(0);"><i class="material-icons">local_atm</i> Financeiro</a></li>
                  <li class = "active"><a style = "color: #777 !important;" href="javascript:void(0);"> Fretes a Pagar</a></li>
              </ol>          
            </div>

            <div  class="row clearfix">
                <!-- Line Chart -->
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id = "data" class="card">
                        <div class="header">
                            <div class = "row">
                                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                    <h2 style= "margin-top: 7px;">Fretes a Pagar</h2>
                                </div>
                            </div>  
                        </div>
                        <div class="body">
                                <div class="row">
                                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box bg-pink hover-expand-effect">
                                          <div class="icon">
                                              <span class="chart chart-line">9,4,6,5,6,4,7,3</span>
                                          </div>
                                          <div class="content">
                                              <div class="text" style = "font-size: 11px;">TOTAL VENCIDAS</div>
                                              <div class="number" style = "font-size: 16px;">
                                                <?php 
                                                  foreach ($total_vencidos as $vencido): 
                                                    echo 'R$ '.number_format($vencido['total_vencidas'], 2, ',', '.');
                                                  endforeach;
                                                ?>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box bg-orange hover-expand-effect">
                                          <div class="icon">
                                              <span class="chart chart-line">9,4,6,5,6,4,7,3</span>
                                          </div>
                                          <div class="content">
                                              <div class="text" style = "font-size: 11px;">A VENCER NO MÊS</div>
                                              <div class="number" style = "font-size: 16px;">
                                                <?php 
                                                   foreach ($total_mes as $vencido): 
                                                    echo 'R$ '.number_format($vencido['total_vencidas_mes'], 2, ',', '.');
                                                  endforeach;
                                                ?>
                                            </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box bg-cyan hover-expand-effect">
                                          <div class="icon">
                                              <div class="chart chart-bar">4,6,-3,-1,2,-2,4,3,6,7,-2,3</div>
                                          </div>
                                          <div class="content">
                                              <div class="text" style = "font-size: 11px;">A VENCER NA SEMANA</div>
                                              <div class="number" style = "font-size: 16px;">
                                                <?php 
                                                  foreach ($total_semana as $vencido): 
                                                    echo 'R$ '.number_format($vencido['total_vencidas_semana'], 2, ',', '.');
                                                  endforeach;
                                                ?>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                      <div class="info-box bg-light-green hover-expand-effect">
                                          <div class="icon">
                                              <div class="chart chart-bar">4,6,-3,-1,2,-2,4,3,6,7,-2,3</div>
                                          </div>
                                          <div class="content">
                                              <div class="text" style = "font-size: 11px;">VENCENDO HOJE</div>
                                              <div class="number" style = "font-size: 16px;">
                                                <?php 
                                                 foreach ($total_dia as $vencido): 
                                                    echo 'R$'.number_format($vencido['total_vencidas_dia'], 2, ',', '.');
                                                 endforeach;
                                                ?>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                            <div class="table-responsive">
                                <table id="duplicatas" class="table table-bordered table-striped table-hover js-basic-example dataTable" style = "font-size: 11px">
                                <thead>
                                            <tr>
                                                <th>Emisão</th>
                                                <th>S</th>
                                                <th>T</th>
                                                <th>Filial</th>
                                                <th>Transportadora</th>
                                                <th>Descrição</th>
                                                <th>Fatura</th>
                                                <th>NF-e</th>
                                                <th>R$ Frete</th>
                                                <th>Vencimento</th>
                                                <th>Competência</th>
                                                <th>Prev. Chegada</th>
                                                <th>Dias Restante</th>
                                            </tr>
                                          </thead>
                                          <tdody>
                                          </tbody>
                                </table>
                            </div>

                        </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Line Chart -->
            </div>
        </div>
    </section>        

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

  <!-- Datatables -->
   <!-- Jquery DataTable Plugin Js -->
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>build/plugins/datatables/keytable/js/dataTables.keyTable.js"></script>
    <script src="<?php echo base_url(); ?>build/js/financiero.js"></script>

    <script type="text/javascript" charset="utf8">
    $(document).ready(function() {
      var url_base = '<?php echo base_url();?>';
        $('#duplicatas').DataTable({
          responsive: true,
          "language": {
                "search": "Pesquisar",
                "lengthMenu": "Mostrando  _MENU_ registros",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Mostrando página 0 de 0",
                "infoFiltered": "(Filtrado de _MAX_ entradas)",
                "loadingRecords": "Carregando...",
                "processing": "Processando...",
                "zeroRecords": "Nenhum resultado encontrado",
                "paginate": {
                    "first":"Primeiro",
                    "previous":"Anterior",
                    "next": "Seguinte",
                    "last": "Ultimo"
                }
            },
            "ajax": {
              "url": url_base + "financiero/CargarDuplicatas",
              "type": "POST",
              "dataSrc": ""
            },
            "columns": [
              {"data": "Data_Emissao"},
              {"data": "Situacao"},
              {"data": "Tipo_Conta"},
              {"data": "Filial"},
              {"data": "Fornecedor"},
              {"data": "Descricao"},
              {"data": "Fatura"},
              {"data": "Nota"},
              {"data": "Valor_Total"},
              {"data": "Data_Vencimento"},
              {"data": "Data_Competencia"},
              {"data": null, 
                "render": function ( data, type, row ) {
                    if (data.Data_Previsao.length > 0){
                      return data.Data_Previsao;
                    }else{
                      return ''; //'<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>'+ data.Ordem 
                    }
                },},
              {"data": "Dias_Restantes"}
            ],
            "keys": true,
            "order": [[ 9, "desc" ]],
            "select": true
        });
       
    } );
    </script>

   