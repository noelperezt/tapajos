<section class="content">
        <div  class="container-fluid">

            <div class="body">
              <ol class="breadcrumb breadcrumb-bg-white breadcrumb-col-red align-right">
                  <li><a href="javascript:void(0);"><i class="material-icons">local_atm</i> Financeiro</a></li>
                  <li class = "active"><a style = "color: #777 !important;" href="javascript:void(0);"> Vendas por Período Hora</a></li>
              </ol>          
            </div>

            <div  class="row clearfix">
                <!-- Line Chart -->
                <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id = "data" class="card">
                        <div class="header">
                            <div class = "row">
                                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                                    <h2 style= "margin-top: 7px;">Relatorio por Hora</h2>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div id="dateranger" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        <span>2015</span> <b class="caret"></b>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <div class="body">
                            <div class = "row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div id= "lista_detalle">
                                    <table id="detalle" class="table table-striped jambo_table bulk_action" cellspacing="0" width="100%" style="font-size: 11px;">
                                        <thead>
                                            <tr>
                                                <th>Forma de Pagamento</th>
                                                <th>Geral R$</th>
                                            </tr>
                                        </thead>
                                        <tdody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
				                        </div>	
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div id="grafico_hora" style="height:350px;"></div>
                            </div>
                            </div>
                        <div class = "row" >
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div id= "lista_filias">
                                    <table id="filiais" class="table table-striped jambo_table bulk_action" cellspacing="0" width="100%" style="font-size: 11px;">
                                        <thead>
                                            <tr>
                                                <th>Filial</th>
                                                <th>Geral R$</th>
                                            </tr>
                                        </thead>
                                        <tdody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Total</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
				                        </div>	
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div id="grafico_filial" style="height:350px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Line Chart -->
            </div>
        </div>
    </section>
 
 <!-- ECharts -->
 <script src="<?php echo base_url(); ?>build/plugins/echarts/dist/echarts.min.js"></script>
   
   <!-- Datatables -->
  <!-- Jquery DataTable Plugin Js -->
  <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/jquery.dataTables.js"></script>
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
   <script src="<?php echo base_url(); ?>build/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
   <script src="<?php echo base_url(); ?>build/plugins/datatables/keytable/js/dataTables.keyTable.js"></script>
   <script src="<?php echo base_url(); ?>build/js/financiero.js"></script>

<script type="text/javascript">
$(function() {

    var start = moment().set({ hours: 08, minutes: 00 });
    var end = moment().set({ hours: 19, minutes: 00 });

    RelatorioPorHora(start.format('MM-DD-YYYY_HH:mm:ss'),end.format('MM-DD-YYYY_HH:mm:ss'));
    cb(start, end);

    function cb(start, end) {
        $('#dateranger span').html(start.format('DD/MM/YYYY HH:mm') + ' - ' + end.format('DD/MM/YYYY HH:mm'));
    }

    $('#dateranger').daterangepicker({
        timePicker: true,
        timePicker24Hour: true,
        startDate: start,
        endDate: end,
        ranges: {
           'Hoje': [moment().set({ hours: 08, minutes: 00 }), moment().set({ hours: 19, minutes: 00 })],
           'Ontem': [moment().subtract(1, 'days').set({ hours: 08, minutes: 00 }), moment().subtract(1, 'days').set({ hours: 19, minutes: 00 })],
           'Últimos 7 dias': [moment().subtract(6, 'days').set({ hours: 08, minutes: 00 }), moment().set({ hours: 19, minutes: 00 })],
           'Últimos 30 dias': [moment().subtract(29, 'days').set({ hours: 08, minutes: 00 }), moment().set({ hours: 19, minutes: 00 })],
           'Mês atual': [moment().startOf('month').set({ hours: 08, minutes: 00 }), moment().endOf('month').set({ hours: 19, minutes: 00 })],
           'Mês passado': [moment().subtract(1, 'month').startOf('month').set({ hours: 08, minutes: 00 }), moment().subtract(1, 'month').endOf('month').set({ hours: 19, minutes: 00 })]
        },
			  locale: {
                format: 'DD/MM/YYYY',
				applyLabel: 'Aplicar',
				cancelLabel: 'Cancelar',
				fromLabel: 'Para',
				toLabel: 'a',
				customRangeLabel: 'Personalizado',
				daysOfWeek: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
				monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                firstDay: 1
              }
    }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD HH:mm') + ' to ' + end.format('YYYY-MM-DD HH:mm:ss'));
                RelatorioPorHora(start.format('MM-DD-YYYY_HH:mm:ss'),end.format('MM-DD-YYYY_HH:mm:ss'));
                cb(start, end);
            });
  

});
</script>

    <?php
        require_once('build/ajax/relatoriohora.php');
    ?>
 

   