<section class="content">
        <div class="container-fluid">

            <div class="body">
              <ol class="breadcrumb breadcrumb-bg-white breadcrumb-col-red align-right">
                  <li class = "active"><a href="javascript:void(0);"><i class="material-icons">home</i> Principal</a></li>
              </ol>          
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <span class="chart chart-line">9,4,6,5,6,4,7,3</span>
                        </div>
                        <div class="content">
                            <div class="text">VENDAS DO ANO</div>
                            <div class="number" style = "font-size: 16px;">
                              <?php 
                                foreach ($ventas_ano as $resultado): 
                                    echo 'R$ '.number_format($resultado['Total_Ano'], 2, ',', '.');
                                endforeach;
                              ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <span class="chart chart-line">9,4,6,5,6,4,7,3</span>
                        </div>
                        <div class="content">
                            <div class="text">VENDAS DO MÊS</div>
                            <div class="number" style = "font-size: 16px;">
                              <?php 
                                foreach ($ventas_mes as $resultado): 
                                  echo 'R$ '.number_format($resultado['Total_Mes'], 2, ',', '.');
                                endforeach;
                              ?>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <div class="chart chart-bar">4,6,-3,-1,2,-2,4,3,6,7,-2,3</div>
                        </div>
                        <div class="content">
                            <div class="text">CONTAS A PAGAR</div>
                            <div class="number" style = "font-size: 16px;">
                              <?php 
                                foreach ($contas_pagar as $resultado): 
                                  echo 'R$ '.number_format($resultado['Pagar'], 2, ',', '.');
                                endforeach;
                              ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <div class="chart chart-bar">4,6,-3,-1,2,-2,4,3,6,7,-2,3</div>
                        </div>
                        <div class="content">
                            <div class="text">CONTAS A RECEBER</div>
                            <div class="number" style = "font-size: 16px;">
                              <?php 
                                foreach ($contas_receber as $resultado): 
                                    echo 'R$ '.number_format($resultado['Receber'], 2, ',', '.');
                                endforeach;
                              ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <!-- Line Chart -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id= "ranking" class="card">
                        <div class="header">
                            <div class = "row">
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                    <h2 style= "margin-top: 7px;">Ranking de Filiais</h2>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <div id="dateranger" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        <span>2015</span> <b class="caret"></b>
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <div class="body">
                            <canvas id="canvas" height="150"></canvas>
                        </div>
                    </div>
                </div>
                <!-- #END# Line Chart -->
            </div>
            <div class="row clearfix">
                <!-- Task Info -->
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                    <div class="card">
                        <div class="header">
                            <h2>Top Vendedores</h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover dashboard-task-infos">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th># De Vendas</th>
                                            <th>Total R$</th>
                                            <th>Grafico de Vendas</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $total = 0;
                                        foreach($resultados as $resultado){
                                            $total = $total + $resultado['Venta'];
                                        }
                                        $array = array("bg-green", "bg-blue", "bg-light-blue", "bg-orange", "bg-red");
                                        $i = 0;
                                        foreach ($resultados as $resultado): ?>
                                             <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $resultado['Nome'] ?></td>
                                                <td><span class="label <?php echo $array[$i] ?>"><?php echo $resultado['Cantidad'] ?></span></td>
                                                <td><?php echo number_format($resultado['Venta'], 2,",",".") ?></td>
                                                <td>
                                                    <div class="progress"  data-toggle="tooltip" data-placement="top" title=" <?php echo round(($resultado['Venta']*100)/$total) ?>%">
                                                        <div  class="<?php echo $array[$i] ?> progress-bar " role="progressbar" aria-valuenow="<?php echo round(($resultado['Venta']*100)/$total) ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo round(($resultado['Venta']*100)/$total) ?>%; background-image: none;"></div>
                                                    </div>
                                                </td>
                                            </tr>
                                    <?php $i++; endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- #END# Task Info -->
                <!-- Browser Usage -->
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="card">
                        <div class="header">
                            <h2>Numero de Vendas</h2>
                        </div>
                        <div class="body">
                            <div id="donut_chart" class="dashboard-donut-chart"></div>
                        </div>
                    </div>
                </div>
                <!-- #END# Browser Usage -->
            </div>
        </div>
    </section>


<script type="text/javascript">
$(function() {

    var start = moment();
    var end = moment();

    //RelatorioPorHora(start.format('MM-DD-YYYY_HH:mm:ss'),end.format('MM-DD-YYYY_HH:mm:ss'));
    cb(start, end);

    function cb(start, end) {
        $('#dateranger span').html(start.year());
    }

    $('#dateranger').daterangepicker({
        showCustomRangeLabel: false,
        startDate: start,
        endDate: end,
        ranges: {
           'Ano Atual': [moment()],
           'Ano Anterior ': [moment().subtract(1, 'years')],
           '2 Anos Atras': [moment().subtract(2, 'years')],
           '3 Anos Atras': [moment().subtract(3, 'years')]
        },
			  locale: {
                format: 'DD/MM/YYYY',
				applyLabel: 'Aplicar',
				cancelLabel: 'Cancelar',
				fromLabel: 'Para',
				toLabel: 'a',
				customRangeLabel: 'Personalizado',
				daysOfWeek: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
				monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                firstDay: 1
              }
    }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD HH:mm') + ' to ' + end.format('YYYY-MM-DD HH:mm:ss'));
                VentasPorAno(start.year());
                cb(start, end);
            });
  

});
</script>
<?php
        require_once('build/ajax/dashboard.php');
    ?>