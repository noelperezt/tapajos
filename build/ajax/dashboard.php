<script type="text/javascript">
    $(document).on("ready",inicio);

    function inicio(){
        var data = [];
        CargaGrafico(data,2018);
        VentasPorAno(2018);
        initDonutChart();
    }
    
    function VentasPorAno(ano){
        StartLoading('#ranking');
        var url_base = '<?php echo base_url();?>';
         $.ajax({
            url: url_base + "dashboard/VentasPorAno/"+ano,
            type: "POST",
            async: true,
            success: function (data){
               var json = JSON.parse(data);
                $(document).ready(function() {
                    var data1 = [];
                    var data2 = [];
                    var data3 = [];
                    var data4 = [];

                    for(i = 0; i < 12; i++){
                        data1.push(json[0].Valores[i].Total);
                        data2.push(json[1].Valores[i].Total);
                        data3.push(json[2].Valores[i].Total);
                        data4.push(json[3].Valores[i].Total);
                    } 

                    var data = [{
                                label: json[0].Filial.toUpperCase(),
                                backgroundColor: "rgba(233, 30, 99,1)",
                                borderColor: "rgba(233, 30, 99,1)",
                                data: data1,
                                fill: false,
                            }, {
                                label: json[1].Filial.toUpperCase(),
                                fill: false,
                                backgroundColor: "rgba(0, 188, 212,1)",
                                borderColor: "rgba(0, 188, 212,1)",
                                data: data2,
                            },{
                                label: json[2].Filial.toUpperCase(),
                                fill: false,
                                backgroundColor: "rgba(139, 195, 74,1)",
                                borderColor: "rgba(139, 195, 74,1)",
                                data: data3,
                            },
                            {
                                label: json[3].Filial.toUpperCase(),
                                fill: false,
                                backgroundColor: "rgba(255, 152, 0,1)",
                                borderColor: "rgba(255, 152, 0,1)",
                                data: data4,
                            }];
                            EndLoading('#ranking');
                            CargaGrafico(data,ano);       
                });
            }
         })
    }   
    
    function CargaGrafico(data,ano){
        var MONTHS = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
                    var config = {
                        type: 'line',
                        data: {
                            labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                            datasets: data
                        },
                        options: {
                            responsive: true,
                            tooltips: {
                                mode: 'index',
                                intersect: false,
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            },
                            scales: {
                                xAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Mes'
                                    }
                                }],
                                yAxes: [{
                                    display: true,
                                    scaleLabel: {
                                        display: true,
                                        labelString: 'Valor (R$)'
                                    }
                                }]
                            }
                        }
                    };

                        var ctx = document.getElementById('canvas').getContext('2d');
                        if (window.myLine) {
	                        window.myLine.clear();
	                        window.myLine.destroy();
                        }
                        window.myLine = new Chart(ctx, config);

                        if(ano == moment().year()){
                            var ahora = moment();
                            var mes = ahora.format('MM');
                            var resta = 12 - mes;
                            var neg = -(resta);

                            console.log(ahora.format('MM'));
                            console.log(resta);

                            config.data.labels.splice(neg, resta); // remove the label first

			                config.data.datasets.forEach(function(dataset) {
				            dataset.data.pop();
			                });

			                window.myLine.update();
                        }                
    }

    function initDonutChart() {
        Morris.Donut({
            element: 'donut_chart',
            data: [
                <?php 
                    $total = 0;
                    foreach($resultados as $resultado){
                        $total = $total + $resultado['Cantidad'];
                    }
                    foreach ($resultados as $resultado): ?>
                        {label: '<?php echo $resultado['Nome'] ?>',
                        value: <?php echo round(($resultado['Cantidad']*100)/$total) ?>},
                    <?php endforeach; ?> 
                ],
            colors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)'],
            formatter: function (y) {
                return y + '%'
            }
        });
    }

</script>