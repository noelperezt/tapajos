<script type="text/javascript"> 
    $(document).on("ready",inicio);

    function inicio(){
        /*$(function() {
            $('#dateranger').daterangepicker({
            opens: 'left',
            format: 'DD/MM/YYYY',
			  separator: ' a ',
			  locale: {
				applyLabel: 'Aplicar',
				cancelLabel: 'Cancelar',
				fromLabel: 'Para',
				toLabel: 'a',
				customRangeLabel: 'Pesquizar',
				daysOfWeek: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
				monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                firstDay: 1
              }
            }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                RelatorioPorHora(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));
            });
            RelatorioPorHora('2018-07-05','2018-07-05');
        });*/
    }

    function RelatorioPorHora(inicio,fin){
        StartLoading('#data');
        var url_base = '<?php echo base_url();?>';
         $.ajax({
            url: url_base + "financiero/ConsultaRelatorioPorHora/"+inicio+"/"+fin,
            type: "POST",
            async: true,
            success: function (data){
               var json = JSON.parse(data);
                $(document).ready(function() {
                    $("#tercero").html('R$ '+ number_format(json.General[0].Total, 2));
                    $('#detalle').DataTable({
                        "paging": false,
                        "ordering": true,
                        "info": false,
                        "searching": false,
                        "language": {
                            "search": "Pesquisar",
                            "lengthMenu": "Mostrando  _MENU_ registros",
                            "info": "Mostrando página _PAGE_ de _PAGES_",
                            "infoEmpty": "Mostrando página 0 de 0",
                            "infoFiltered": "(Filtrado de _MAX_ entradas)",
                            "loadingRecords": "Carregando...",
                            "processing": "Processando...",
                            "zeroRecords": "Nenhum resultado encontrado",
                            "decimal": ",",
                            "thousands": ".",
                            "paginate": {
                                "first":"Primeiro",
                                "previous":"Anterior",
                                "next": "Seguinte",
                                "last": "Ultimo"
                            }   
                        },
                        "keys": true,
                        "order": [[ 1, "desc" ]],
                        "select": true,
                        "data" : json.Detalle,
                        "columns": [
                            {"data": "name"},
                            {"data": "value", 
                                "render": function ( d ) {
                                    if(d != null){
                                        return number_format(d, 2);
                                    }else{
                                        return '0,00'
                                    } 
                            },}
                            ],
                            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // computing column Total the complete result 
            var monTotal = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return parseFloat(a) + parseFloat(b);
                }, 0 );
				
			 
			
				
            // Update footer by showing the total with the reference of the column index 
			$( api.column( 0 ).footer() ).html('Total');
            $( api.column( 1 ).footer() ).html(number_format(monTotal,2));
        },
                            "destroy": true
                    });  

                    $('#filiais').DataTable({
                        "paging": false,
                        "ordering": true,
                        "info": false,
                        "searching": false,
                        "language": {
                            "search": "Pesquisar",
                            "lengthMenu": "Mostrando  _MENU_ registros",
                            "info": "Mostrando página _PAGE_ de _PAGES_",
                            "infoEmpty": "Mostrando página 0 de 0",
                            "infoFiltered": "(Filtrado de _MAX_ entradas)",
                            "loadingRecords": "Carregando...",
                            "processing": "Processando...",
                            "zeroRecords": "Nenhum resultado encontrado",
                            "decimal": ",",
                            "thousands": ".",
                            "paginate": {
                                "first":"Primeiro",
                                "previous":"Anterior",
                                "next": "Seguinte",
                                "last": "Ultimo"
                            }   
                        },
                        "keys": true,
                        "order": [[ 1, "desc" ]],
                        "select": true,
                        "data" : json.Filiais,
                        "columns": [
                            {"data": "name"},
                            {"data": "value", 
                                "render": function ( d ) {
                                    if(d != null){
                                        return number_format(d, 2);
                                    }else{
                                        return '0,00'
                                    } 
                            },}
                            ],
                            "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // computing column Total the complete result 
            var monTotal = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return parseFloat(a) + parseFloat(b);
                }, 0 );
				
			 
			
				
            // Update footer by showing the total with the reference of the column index 
			$( api.column( 0 ).footer() ).html('Total');
            $( api.column( 1 ).footer() ).html(number_format(monTotal,2));
        },
                            "destroy": true
                    });  
                    
                    function init_echarts() {
                        if ($('#grafico_hora').length ){ 
			                var echartPieCollapse = echarts.init(document.getElementById('grafico_hora'), theme);
			  
			                echartPieCollapse.setOption({
				                tooltip: {
				                    trigger: 'item',
				                    formatter: "{a} <br/>{b} : {c} ({d}%)"
				                },
				                legend: {
				                    x: 'center',
				                    y: 'bottom',
				                    data: json.Detalle
				                },
				                toolbox: {
				                    show: true,
				                    feature: {
					                    magicType: {
					                        show: true,
					                        type: ['pie', 'funnel']
					                    },
					                    restore: {
					                         show: true,
					                        title: "Restore"
					                    },
					                    saveAsImage: {
					                        show: true,
					                        title: "Save Image"
					                    }
				                    }
				                },
				                calculable: true,
				                series: [{
				                    name: 'Forma de Pagamento',
				                    type: 'pie',
				                    radius: [25, 90],
				                    center: ['50%', 170],
				                    roseType: 'area',
				                    x: '50%',
				                    max: RetornaCero(json.General[0].total),
				                    sort: 'ascending',
				                    data: json.Detalle
				                }]
			                });
			            }

                        if ($('#grafico_filial').length ){ 
			                var echartPieCollapse = echarts.init(document.getElementById('grafico_filial'), theme);
			  
			                echartPieCollapse.setOption({
				                tooltip: {
				                    trigger: 'item',
				                    formatter: "{a} <br/>{b} : {c} ({d}%)"
				                },
				                legend: {
				                    x: 'center',
				                    y: 'bottom',
				                    data: json.Filiais
				                },
				                toolbox: {
				                    show: true,
				                    feature: {
					                    magicType: {
					                        show: true,
					                        type: ['pie', 'funnel']
					                    },
					                    restore: {
					                         show: true,
					                        title: "Restore"
					                    },
					                    saveAsImage: {
					                        show: true,
					                        title: "Save Image"
					                    }
				                    }
				                },
				                calculable: true,
				                series: [{
				                    name: 'Filial',
				                    type: 'pie',
				                    radius: [25, 90],
				                    center: ['50%', 170],
				                    roseType: 'area',
				                    x: '50%',
				                    sort: 'ascending',
				                    data: json.Filiais
				                }]
			                });
			            }  
                    }

                    init_echarts();
                    EndLoading('#data');
                });
            }
        })
    }
</script>








