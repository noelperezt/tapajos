<script type="text/javascript"> 
    var grafico = null;
    $(document).on("ready",inicio);

    function inicio(){
        /*$(function() {
            $('#dateranger').daterangepicker({
            opens: 'left',
            format: 'DD/MM/YYYY',
			  separator: ' a ',
			  locale: {
				applyLabel: 'Aplicar',
				cancelLabel: 'Cancelar',
				fromLabel: 'Para',
				toLabel: 'a',
				customRangeLabel: 'Pesquizar',
				daysOfWeek: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
				monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                firstDay: 1
              }
            }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                RelatorioPorHora(start.format('YYYY-MM-DD'),end.format('YYYY-MM-DD'));
            });
            RelatorioPorHora('2018-07-05','2018-07-05');
        });*/
    }

    function RelatorioPorData(inicio,fin){
        StartLoading('#data');
        var url_base = '<?php echo base_url();?>';
         $.ajax({
            url: url_base + "financiero/ConsultaRelatorioPorData/"+inicio+"/"+fin,
            type: "POST",
            async: true,
            success: function (data){
               var json = JSON.parse(data);
                $(document).ready(function() {
                    $("#primero").html('R$ '+ number_format(json.General[0].Manana, 2));
                    $("#segundo").html('R$ '+ number_format(json.General[0].Tarde, 2));
                    $("#tercero").html('R$ '+ number_format(json.General[0].Total, 2));
                    $('#detalle').DataTable({
                        "paging": false,
                        "ordering": true,
                        "info": false,
                        "searching": false,
                        "language": {
                            "search": "Pesquisar",
                            "lengthMenu": "Mostrando  _MENU_ registros",
                            "info": "Mostrando página _PAGE_ de _PAGES_",
                            "infoEmpty": "Mostrando página 0 de 0",
                            "infoFiltered": "(Filtrado de _MAX_ entradas)",
                            "loadingRecords": "Carregando...",
                            "processing": "Processando...",
                            "zeroRecords": "Nenhum resultado encontrado",
                            "decimal": ",",
                            "thousands": ".",
                            "paginate": {
                                "first":"Primeiro",
                                "previous":"Anterior",
                                "next": "Seguinte",
                                "last": "Ultimo"
                            }   
                        },
                        "keys": true,
                        "order": [[ 3, "desc" ]],
                        "select": true,
                        "data" : json.Detalle,
                        "columns": [
                            {"data": "Nome"},
                            {"data": "Manana", 
                                "render": function ( d ) {
                                    if(d != null){
                                        return number_format(d, 2);
                                    }else{
                                        return '0,00'
                                    } 
                            },},
                            {"data": "Tarde", 
                                "render": function ( d ) {
                                    if(d != null){
                                        return number_format(d, 2);
                                    }else{
                                        return '0,00'
                                    } 
                            },},
                            {"data": "Total", 
                                "render": function ( d ) {
                                    if(d != null){
                                        return number_format(d, 2);
                                    }else{
                                        return '0,00'
                                    } 
                            },}
                            ],
                            "destroy": true
                    });  

                    $('#filiais').DataTable({
                        "paging": false,
                        "ordering": true,
                        "info": false,
                        "searching": false,
                        "language": {
                            "search": "Pesquisar",
                            "lengthMenu": "Mostrando  _MENU_ registros",
                            "info": "Mostrando página _PAGE_ de _PAGES_",
                            "infoEmpty": "Mostrando página 0 de 0",
                            "infoFiltered": "(Filtrado de _MAX_ entradas)",
                            "loadingRecords": "Carregando...",
                            "processing": "Processando...",
                            "zeroRecords": "Nenhum resultado encontrado",
                            "decimal": ",",
                            "thousands": ".",
                            "paginate": {
                                "first":"Primeiro",
                                "previous":"Anterior",
                                "next": "Seguinte",
                                "last": "Ultimo"
                            }   
                        },
                        "keys": true,
                        "select": true,
                        "data" : json.Filiais,
                        "columns": [
                            {"data": "Nome"},
                            {"data": "Manana", 
                                "render": function ( d ) {
                                    if(d != null){
                                        return number_format(d, 2);
                                    }else{
                                        return '0,00'
                                    } 
                            },},
                            {"data": "Tarde", 
                                "render": function ( d ) {
                                    if(d != null){
                                        return number_format(d, 2);
                                    }else{
                                        return '0,00'
                                    } 
                            },},
                            {"data": "Total", 
                                "render": function ( d ) {
                                    if(d != null){
                                        return number_format(d, 2);
                                    }else{
                                        return '0,00'
                                    } 
                            },}
                            ],
                            "destroy": true,
                            "order": [[ 3, "desc" ]]
                    });  
                    
                        if ($('#grafico_hora').length ){ 
			                var echartPieCollapse = echarts.init(document.getElementById('grafico_hora'), theme);
			  
			                echartPieCollapse.setOption({
				                tooltip: {
				                    trigger: 'item',
				                    formatter: "{a} <br/>{b} : {c} ({d}%)"
				                },
				                legend: {
				                    x: 'center',
				                    y: 'bottom',
				                    data: ['Manhã', 'Tarde']
				                },
				                toolbox: {
				                    show: true,
				                    feature: {
					                    magicType: {
					                        show: true,
					                        type: ['pie', 'funnel']
					                    },
					                    restore: {
					                         show: true,
					                        title: "Restore"
					                    },
					                    saveAsImage: {
					                        show: true,
					                        title: "Save Image"
					                    }
				                    }
				                },
				                calculable: true,
				                series: [{
				                    name: 'Periodo',
				                    type: 'pie',
				                    radius: [25, 90],
				                    center: ['50%', 170],
				                    roseType: 'area',
				                    x: '50%',
				                    max: RetornaCero(json.General[0].Total),
				                    sort: 'ascending',
				                    data: [{
					                    value: RetornaCero(json.General[0].Manana),
                                        backgroundColor: "#00BCD4",
					                    name: 'Manhã'
				                    }, {
					                    value: RetornaCero(json.General[0].Tarde),
					                    name: 'Tarde'
				                    }]
				                }]
                            });
                        } 
                        
                        if ($('#grafico_filial').length ){ 

              var nomes = [];
              var mananas = []; 
              var tardes = [];
              
              for (var i in json.Filiais) {
                nomes[i] = json.Filiais[i].Nome;
                mananas[i] = json.Filiais[i].Manana;
                tardes[i] = json.Filiais[i].Tarde;
             }

              var ctx = document.getElementById("grafico_filial");

             if(grafico !== null){
                 grafico.clear();
                 grafico.destroy();
             }
                       
			 grafico = new Chart(ctx, {
                type: 'bar',
                options: { 
                    scales: { 
                        xAxes: [{ 
                            display: false }] 
                        } 
                    },
				data: {
				  labels: nomes,
				  datasets: [{
					label: 'Manhã',
					backgroundColor: "#00BCD4",
					data: mananas
				  }, {
					label: 'Tarde',
					backgroundColor: "#FF9800",
					data: tardes
				  }]
				},

				options: {
				  scales: {
					yAxes: [{
					  ticks: {
						display: true
					  }
                    }],
                    xAxes: [{ display: false }] 
				  }
				}
              });
                    
                }

             
                });
                EndLoading('#data');
            }
        })
    }
</script>








