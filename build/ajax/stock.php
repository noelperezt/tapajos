<script type="text/javascript"> 

    function ListarStock(filial){
        StartLoading('#data');
        var url_base = '<?php echo base_url();?>';
         $.ajax({
            url: url_base + "stock/ListaStock/" + filial,
            type: "POST",
            async: true,
            success: function (data){
                var json = JSON.parse(data);
                $(document).ready(function() {
                    var table = $('#stock').DataTable({
                        "autoWidth": true,
                        "data" : json,
                        "columns": [
                            {"data": "Codigo"},
                            {"data": "Nome"},
                            {"data": "Estoque_Minimo",
                                "render": function ( d ) {
                                    if(d != null){
                                        return number_format(d, 2);
                                    }else{
                                        return '0,00'
                                    } 
                            },},
                            {"data": "Qtde_Estoque_Atual",
                                "render": function ( d ) {
                                    if(d != null){
                                        return number_format(d, 2);
                                    }else{
                                        return '0,00'
                                    } 
                            },},
                            {"data": "Estoque_Ideal",
                                "render": function ( d ) {
                                    if(d != null){
                                        return number_format(d, 2);
                                    }else{
                                        return '0,00'
                                    } 
                            },}
                            ],
                            "keys":true,
                            "language": {
                                "search": "Pesquisar",
                                "lengthMenu": "Mostrando  _MENU_ registros",
                                "info": "Mostrando página _PAGE_ de _PAGES_",
                                "infoEmpty": "Mostrando página 0 de 0",
                                "infoFiltered": "(Filtrado de _MAX_ entradas)",
                                "loadingRecords": "Carregando...",
                                "processing": "Processando...",
                                "zeroRecords": "Nenhum resultado encontrado",
                                "decimal": ",",
                                "thousands": ".",
                                "paginate": {
                                    "first":"Primeiro",
                                    "previous":"Anterior",
                                    "next": "Seguinte",
                                    "last": "Ultimo"
                                        }   
                                },
                        "order": [[ 1, "desc" ]],
                        "destroy": true
                    });  
                    
                    EndLoading('#data');
                });
            }
        })
    }

</script>