<script type="text/javascript"> 
    $(document).on("ready",inicio);

    function inicio(){
        MostrarDuplicatas();
    }

    function MostrarDuplicatas(){
        var url_base = '<?php echo base_url();?>';
         $.ajax({
            url: url_base + "financiero/CargarDuplicatas",
            type: "POST",
            async: true,
            success: function (data){
                var json = JSON.parse(data);
                $(document).ready(function() {
                    $('#duplicatas').DataTable({
                        "data" : json,
                        "columns": [
                            {"data": "Data_Emissao"},
                            {"data": "Situacao"},
                            {"data": "Tipo_Conta"},
                            {"data": "Filial"},
                            {"data": "Fornecedor"},
                            {"data": "Descricao"},
                            {"data": "Fatura"},
                            {"data": "Nota"},
                            {"data": "Valor_Total"},
                            {"data": "Data_Vencimento"},
                            {"data": "Data_Competencia"},
                            {"data": "Data_Previsao"},
                            {"data": "Dias_Restantes"}
                            ],
                        "language": {
                            "search": "Pesquisar.",
                            "lengthMenu": "Mostrando  _MENU_ registros",
                            "info": "Mostrando página _PAGE_ de _PAGES_",
                            "paginate": {
                                "first":"Primeiro",
                                "previous":"Anterior",
                                "next": "Seguinte",
                                "last": "Ultimo"
                            }
                        },
                        "order": [[ 9, "desc" ]]
                    });  
                });
            }
        })
    }

</script>








