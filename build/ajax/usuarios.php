<script type="text/javascript"> 
    var save_method; //for save method string
    var table;

    function add_user(){
      save_method = 'add';
      $('#form')[0].reset();
      $('#modal_form').modal('show'); 
    }

    function edit_user(id){
      var url_base = '<?php echo base_url();?>'
      save_method = 'update';
      $('#form_editar')[0].reset(); // reset form on modals
 
      //Ajax Load data from ajax
      $.ajax({
        url : url_base+"auth/getUsuario/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_user"]').val(data[0].users_id);
            $('[name="user_name"]').val(data[0].username);
            $('[name="nome"]').val(data[0].first_name);
            $('[name="sobrenome"]').val(data[0].last_name);
            $('[name="email"]').val(data[0].email);
 
 
            $('#modal_form_editar').modal('show'); // show bootstrap modal when complete loaded
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
    }

    function cambiarstatus(id){
        var url_base = '<?php echo base_url();?>'

        swal({
            title: "AVISO!",
            text: "Deseja Confirmar a operação?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: 'btn-success',
            confirmButtonText: "sim",
            cancelButtonText: "Cancelar",
            cancelButtonClass: 'btn-danger',
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url : url_base +'auth/CambiarStatus/'+id,
                    type: "POST",
                    data: $(form).serialize(),
                    dataType: "JSON",
                    success: function(data){
                if(data.type == 'success'){
                    swal({
                        title: "CONFIRMADO!",
                        text: 'A Operação foi Realizada!',
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: 'btn-success',
                        confirmButtonText: 'OK',
                         
                    }, function(){ 
                        table.ajax.reload();
                    });            
                }else{
                    swal({
                        title: 'Aviso!',
                        text: data.message,
                        type: 'warning',
                        html :  true,
                        confirmButtonClass: 'btn-warning',
                        confirmButtonText: 'OK', 
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
            });
        } else {
            swal({
                title: 'CANCELADO',
                text: 'A Operação foi cancelada!',
                type: 'error',
                showCancelButton: false,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'OK', 
            });
        }
        });

    }

    function reiniciarsenha(id){
        var url_base = '<?php echo base_url();?>'

        swal({
            title: "AVISO!",
            text: "Deseja Confirmar a operação?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: 'btn-success',
            confirmButtonText: "sim",
            cancelButtonText: "Cancelar",
            cancelButtonClass: 'btn-danger',
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url : url_base +'auth/ReiniciarSenha/'+id,
                    type: "POST",
                    data: $(form).serialize(),
                    dataType: "JSON",
                    success: function(data){
                if(data.type == 'success'){
                    swal({
                        title: "CONFIRMADO!",
                        text: 'A Operação foi Realizada!',
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: 'btn-success',
                        confirmButtonText: 'OK',
                         
                    }, function(){ 
                        table.ajax.reload();
                    });            
                }else{
                    swal({
                        title: 'AVISO!',
                        text: data.message,
                        type: 'warning',
                        html :  true,
                        confirmButtonClass: 'btn-warning',
                        confirmButtonText: 'OK', 
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
            });
        } else {
            swal({
                title: 'CANCELADO',
                text: 'A Operação foi cancelada!',
                type: 'error',
                showCancelButton: false,
                confirmButtonClass: 'btn-danger',
                confirmButtonText: 'OK', 
            });
        }
        });

    }
    
    function save(){
      var url_base = '<?php echo base_url();?>'
      var url;
      var texto;
      var form;
      var modal;
      if(save_method == 'add')
      {
          url = url_base+"auth/register";
          texto = 'Usuário cadastrado com sucesso';
          form ='#form';
          modal = '#modal_form'
      }
      else
      {
        url = url_base+"auth/update";
        texto = 'Usuário mudado com sucesso';
        form ='#form_editar';
        modal = '#modal_form_editar'
      }
 
       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $(form).serialize(),
            dataType: "JSON",
            success: function(data){
                if(data.notif.type == 'success'){
                    $(modal).modal('hide');
                    swal({
                        title: "CONFIRMADO!",
                        text: texto,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonClass: 'btn-success',
                        confirmButtonText: 'Aceitar',
                         
                    }, function(){ 
                        table.ajax.reload();
                    });            
                }else{
                    swal({
                        title: 'AVISO!',
                        text: data.notif.message,
                        type: 'warning',
                        html :  true,
                        confirmButtonClass: 'btn-warning',
                        confirmButtonText: 'Aceitar', 
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
            }
        });
    }
</script>








