var CURRENT_URL = window.location.href.split('#')[0].split('?')[0];
var $BODY = $('body');
var $SIDEBAR_MENU = $('#sidebar-menu');

$SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('active');
$SIDEBAR_MENU.find('a').filter(function () {
	return this.href == CURRENT_URL;
}).parent('li').addClass('active').parents('ul').slideDown().parent().addClass('active');
console.log(CURRENT_URL);

function StartLoading(elemento){
	$(elemento).waitMe({
		effect: 'orbit',
		text: 'Carregando...',
		bg: 'rgba(255,255,255,0.90)',
		color: '#555'
	});
}

function EndLoading(elemento){
	$(elemento).waitMe('hide');
}